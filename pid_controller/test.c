#include "greatest.h"
#include "pid_controller.h"

TEST test_pid_step_response(void)
{
    pid_t pid;

    pid_init(&pid, 1.0, 0.5, 1.0, -2, 2, 0.2);

    float control_value = 0;
    float process_value = 0;

    pid_set_point(&pid, 0.0);

    for (int i = 0; i < 20; ++i)
    {
        if (i == 2)
        {
            pid_set_point(&pid, 1.0);
        }

        process_value = control_value * 0.5;
        control_value = pid_process(&pid, process_value, 1);

        printf("process_value: %f, set_point: %f, control_value: %f\n", process_value, pid.set_point, control_value);
    }

    // ASSERT_EQ_FMT(-31, data[0], "%d");

    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TEST(test_pid_step_response);

    GREATEST_MAIN_END();
}
