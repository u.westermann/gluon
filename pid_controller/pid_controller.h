/** \file
 * PID controller.
 *
 * https://en.wikipedia.org/wiki/PID_controller
 */

#ifndef PID_CONTROLLER_H
#define PID_CONTROLLER_H

/** CONFIGURATION: Define for header only library, undefine for standalone
 * module. */
//#define PID_CONTROLLER_HEADER_ONLY

#ifdef PID_CONTROLLER_HEADER_ONLY
#define PID_CONTROLLER_SPEC static inline
#else
#define PID_CONTROLLER_SPEC
#endif

typedef struct
{
    float set_point;
    float kp;
    float ki;
    float kd;
    float min_output_limit;
    float max_output_limit;
    float acceptable_error;
    float integral;
    float last_error;
} pid_t;

PID_CONTROLLER_SPEC void pid_init(pid_t* pid, float kp, float ki, float kd,
                                  float min_output_limit,
                                  float max_output_limit,
                                  float acceptable_error);
PID_CONTROLLER_SPEC void pid_set_point(pid_t* pid, float set_point);
PID_CONTROLLER_SPEC float pid_process(pid_t* pid, float process_value,
                                      float dt);

#ifdef PID_CONTROLLER_HEADER_ONLY
#include "pid_controller.c"
#endif

#endif
