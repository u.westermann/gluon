#ifndef PID_CONTROLLER_HEADER_ONLY
#include "pid_controller.h"
#endif

#include <assert.h>
#include <math.h>
#include <stddef.h>

PID_CONTROLLER_SPEC void pid_init(pid_t* pid, float kp, float ki, float kd,
                                  float min_output_limit,
                                  float max_output_limit,
                                  float acceptable_error)
{
    assert(pid != NULL);
    assert(min_output_limit < max_output_limit);

    pid->set_point = 0.0;
    pid->kp = kp;
    pid->ki = ki;
    pid->kd = kd;
    pid->min_output_limit = min_output_limit;
    pid->max_output_limit = max_output_limit;
    pid->acceptable_error = acceptable_error;
    pid->integral = 0.0;
    pid->last_error = 0.0;
}

PID_CONTROLLER_SPEC void pid_set_point(pid_t* pid, float set_point)
{
    pid->set_point = set_point;
}

PID_CONTROLLER_SPEC float pid_process(pid_t* pid, float measured_process_value,
                                      float dt)
{
    assert(pid != NULL);
    assert(dt != 0.0);

    float error = pid->set_point - measured_process_value;

    if (error < fabsf(pid->acceptable_error))
    {
        error = 0;
    }

    pid->integral = pid->integral + error * dt;

    float p_term = pid->kp * error;
    float i_term = pid->ki * pid->integral;
    float d_term = pid->kd * (error - pid->last_error) / dt;

    float output = p_term + i_term + d_term;

    if (output >= pid->max_output_limit)
    {
        output = pid->max_output_limit;

        if (i_term > pid->max_output_limit)
        {
            pid->integral = pid->max_output_limit / pid->ki;
        }
    }
    else if (output <= pid->min_output_limit)
    {
        output = pid->min_output_limit;

        if (i_term < pid->min_output_limit)
        {
            pid->integral = pid->min_output_limit / pid->ki;
        }
    }

    pid->last_error = error;

    return output;
}
