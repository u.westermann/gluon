#!/usr/bin/env bash

set -eux

IFS=$'\n'

for folder in $(find . -type d); do 
    if [ -f "$folder"/Makefile ]; then
        make -C "$folder" clean test
    fi
done

echo SUCCESS
