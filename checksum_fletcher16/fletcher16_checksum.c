#ifndef FLETCHER16_CHECKSUM_HEADER_ONLY
#include "fletcher16_checksum.h"
#endif

#include <assert.h>
#include <stddef.h>

FLETCHER16_CHECKSUM_SPEC void
fletcher16_checksum(uint8_t c0_init, uint8_t c1_init, uint8_t* data,
                    uint32_t length, uint8_t* c0_result, uint8_t* c1_result)
{
    assert(data != NULL);

    uint_fast16_t c0 = c0_init;
    uint_fast16_t c1 = c1_init;

    for (uint_fast32_t i = 0; i < length; ++i)
    {
        c0 = (c0 + data[i]) % 0xFF;
        c1 = (c1 + c0) % 0xFF;
    }

    *c0_result = c0;
    *c1_result = c1;
}
