/** \file
 * Calculation of Fletcher16 checksum. Fletcher checksums are faster to
 * calculate than CRCs, but are found to detect less errors.
 *
 * https://en.wikipedia.org/wiki/Fletcher's_checksum
 */

#ifndef FLETCHER16_CHECKSUM_H
#define FLETCHER16_CHECKSUM_H

/** CONFIGURATION: Define for header only library, undefine for standalone
 * module. */
//#define FLETCHER16_CHECKSUM_HEADER_ONLY

#ifdef FLETCHER16_CHECKSUM_HEADER_ONLY
#define FLETCHER16_CHECKSUM_SPEC static inline
#else
#define FLETCHER16_CHECKSUM_SPEC
#endif

#include <stdint.h>

/**
 * Calculate Fletcher16 checksum. Init values are normally set to 0. result
 * arguments contain the result after calculation. The function can be used to
 * calculate the checksum over data of a fixed length. It is also possible to
 * calculate the checksum of a stream of arbitrary length on the fly, using the
 * result from the last calculation as init values for the calculation of the
 * next results.
 */
FLETCHER16_CHECKSUM_SPEC void
fletcher16_checksum(uint8_t c0_init, uint8_t c1_init, uint8_t* data,
                    uint32_t length, uint8_t* c0_result, uint8_t* c1_result);

#ifdef FLETCHER16_CHECKSUM_HEADER_ONLY
#include "fletcher16_checksum.c"
#endif

#endif
