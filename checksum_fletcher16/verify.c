#include "fletcher16_checksum.h"
#include <stdint.h>

void verification_case_checksum(void)
{
    uint8_t test_vector[5];
    uint32_t length = sizeof(test_vector) / sizeof(uint8_t);

    uint8_t c0;
    uint8_t c1;

    fletcher16_checksum(0, 0, test_vector, length, &c0, &c1);
}

int main(void)
{
    verification_case_checksum();

    return 0;
}
