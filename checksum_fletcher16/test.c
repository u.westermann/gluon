#include "fletcher16_checksum.h"
#include "greatest.h"
#include <stdint.h>

TEST test_fletcher16_checksum_block(void)
{
    uint8_t test_vector[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
    uint32_t length = sizeof(test_vector) / sizeof(uint8_t);

    uint8_t c0;
    uint8_t c1;

    fletcher16_checksum(0, 0, test_vector, length, &c0, &c1);

    ASSERT_EQ_FMT(0x27, c0, "c0 == 0x%04X");
    ASSERT_EQ_FMT(0x06, c1, "c1 == 0x%04X");

    PASS();
}

TEST test_fletcher16_checksum_stream(void)
{
    uint8_t test_vector[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
    uint32_t length = sizeof(test_vector) / sizeof(uint8_t);

    uint8_t c0 = 0;
    uint8_t c1 = 0;

    for (int i = 0; i < length; ++i)
    {
        fletcher16_checksum(c0, c1, &test_vector[i], 1, &c0, &c1);
    }

    ASSERT_EQ_FMT(0x27, c0, "c0 == 0x%04X");
    ASSERT_EQ_FMT(0x06, c1, "c1 == 0x%04X");

    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TEST(test_fletcher16_checksum_block);
    RUN_TEST(test_fletcher16_checksum_stream);

    GREATEST_MAIN_END();
}
