# gluon - A toolkit of simple algorithms, data structures and utilities in C.

Full [HTML documentation is here.](https://u.westermann.gitlab.io/gluon/)


## Using gluon

gluon is a growing collection of simple algorithms, data structures and utilities in C. Due to design restrictions (see below), it is even usable (but not restricted to) small embedded systems, microcontrollers or systems without operating system.

Most modules contain no dependencies or only dependencies to parts of the C standard library. Most modules can be used independently from each other.

Unless otherwise noted, none of the algorithms use dynamic memory allocation or I/O streams, since their use can be problematic in constrained environments.

Some modules have a configuration section in their header files. For example, one configuration option that is often included, is to use a module as "header only library". If active, you only have to include the header file to use the module. Of course, this comes with additional cost; each compilation unit will contain a copy of the included functions.

Some modules contain further configuration defines, see the documentation of specific modules for details.

Usage examples can be found in the accompanying test.c files, that contain unit tests.

Happy hacking...


### List of Modules

- 32 bit cyclic redundancy check - crc32.h
- Calculate Fletcher 32 checksum - fletcher32\_checksum.h
- Calculate Fletcher16 checksum - fletcher16\_checksum.h
- Consistent overhead byte stuffing - cobs\_encode.h cobs\_decode.h
- Determine endianess of host system - endian\_check.h
- Encryption/decryption with XTEA cipher - xtea\_cipher.h
- Insertion sort algorithm - insertion\_sort.h
- Macros and functions for bit banging - bit\_hacks.h
- Macros to check result of arithmetic operations
- PID controller - pid\_controller.h
- Ring buffer (aka circular queue) - ring\_buffer.h
- Selection sort algorithm - selection\_sort.h
- Basic singly linked list - list\_basic.h
- Singly linked list with bells and whistles - list.h


## Developing gluon

### Leading Design Principles

- Comprehensibility through simplicity and brevity.
- Correctness, portability and maintainability through comprehensibility.
- Generality
- Usability


### Coding Rules

#### Design

- Use the simplest algorithm that can do the job.
- No fancy speed or size optimizations.
- Keep in mind limited resources of embedded systems (time and space complexity, capabilities of 8/16/32-bit architectures).
- Keep in mind compatibility with little/big endian byte order architectures.
- Design public API for easy use and broad range of use cases.
- Where possible, make algorithms configurable to work with different data types.
- Where possible, provide the optional possibility to use modules as header only library, for ease of use.
- If a module needs to maintain state between invocations, use an explicit "this" argument, to enable multiple instances.
- No use of dynamic memory allocation (`malloc()`, `alloc()`).
- No use of I/O streams (`FILE`, `stdin`, `stdout`, `stderr`).
- No usage of language constructs with undefined or unspecified behaviour. Usage of implementation defined behaviour must be documented.
- Heavy use of `assert()`, also for checking the validity of function parameters.
- Test/verification coverage of all relevant parts.
- Code should be C99 compatible, deviations should be documented.


#### Code Formatting

- File format is UTF-8, Unix file endings (LF), no tabs, 4 spaces for indentation.
- Functions should be short, at least as short as the number of lines that can be displayed on a typical monitor.
- Lines should be short, at least as short as the number of columns that can be displayed on a typical monitor.
- `if`/`else` branches always use `{...}` code blocks.
- Opening braces (`{`) of code blocks are placed always on a new line.
- Operators and expressions are delimited by spaces (e.g. `y = x + 42` as opposed to `y=x+42`).
- Pointer declarations have the star next to the variable type (e.g. `int32\_t* i` as opposed to `int32\_t *i`).
- There are no spaces after opening parentheses (`(`) or before closing parentheses (`)`).
- No spaces after a function name (e.g. `foo()`).
- Use spaces after keywords, except they look like function calls (e.g `if (...)`, `while (...)` but `sizeof()`.
- All names use lower snake\_case.
- Names document the source code. They should be meaningful for developers reading the code for the first time.
- Names should be context sensitive. The general rule is that names get shorter with its degree of locality, since a names context (module or function name) already holds information that does not need to be repeated. As long as necessary, as short as possible.
- Use of code comments is regarded as failure to use proper naming.


## License and Credits

Unless otherwise noted in the specific files, all code is licensed under BSD 2-clause, Copyright (c) Ulf Westermann.

gluon uses the great unit test framework [greatest](https://github.com/silentbicycle/greatest), which is licensed under its own terms.

gluon uses the bounded model checker [CBMC](http://www.cprover.org/cbmc/) for verification. 

Books and resources
- http://hackersdelight.org/
- http://www.algorist.com/
- http://www.numerical.recipes/
- http://www.dspguide.com/
- https://www.embedded.com/user/JackCrens
- http://cppreference.com/
- https://wiki.sei.cmu.edu/confluence/display/c/SEI+CERT+C+Coding+Standard
