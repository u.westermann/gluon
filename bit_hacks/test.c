#include "bit_hacks.h"
#include "greatest.h"
#include <stdint.h>


TEST test_set_bit(void)
{
    uint16_t ui16 = U16_SET_BIT(0);
    ASSERT_EQ_FMT(0x1U, ui16, "0x%X");

    ui16 = U16_SET_BIT(1);
    ASSERT_EQ_FMT(0x2U, ui16, "0x%X");

    ui16 = U16_SET_BIT(15);
    ASSERT_EQ_FMT(0x8000U, ui16, "0x%X");

    uint32_t ui32 = U32_SET_BIT(0);
    ASSERT_EQ_FMT(0x1U, ui32, "0x%X");

    ui32 = U32_SET_BIT(1);
    ASSERT_EQ_FMT(0x2U, ui32, "0x%X");

    ui32 = U32_SET_BIT(31);
    ASSERT_EQ_FMT(0x80000000U, ui32, "0x%X");

    uint64_t ui64 = U64_SET_BIT(0);
    ASSERT_EQ_FMT(0x1UL, ui64, "0x%lX");

    ui64 = U64_SET_BIT(1);
    ASSERT_EQ_FMT(0x2UL, ui64, "0x%lX");

    ui64 = U64_SET_BIT(63);
    ASSERT_EQ_FMT(0x8000000000000000UL, ui64, "0x%lX");

    PASS();
}


TEST test_is_power_of_two(void)
{
    ASSERT_EQ_FMT(0, IS_POWER_OF_TWO(0), "%d");
    ASSERT_EQ_FMT(1, IS_POWER_OF_TWO(1), "%d");
    ASSERT_EQ_FMT(1, IS_POWER_OF_TWO(2), "%d");
    ASSERT_EQ_FMT(0, IS_POWER_OF_TWO(3), "%d");
    ASSERT_EQ_FMT(1, IS_POWER_OF_TWO(4), "%d");
    ASSERT_EQ_FMT(0, IS_POWER_OF_TWO(5), "%d");
    ASSERT_EQ_FMT(0, IS_POWER_OF_TWO(127), "%d");
    ASSERT_EQ_FMT(1, IS_POWER_OF_TWO(128), "%d");
    ASSERT_EQ_FMT(0, IS_POWER_OF_TWO(129), "%d");
    ASSERT_EQ_FMT(1, IS_POWER_OF_TWO(0x80000000U), "%d");

    PASS();
}


TEST test_u16_set_bit(void)
{
    ASSERT_EQ_FMT(0x1, U16_SET_BIT(0), "0x%X");
    ASSERT_EQ_FMT(0x2, U16_SET_BIT(1), "0x%X");
    ASSERT_EQ_FMT(0x4000, U16_SET_BIT(14), "0x%X");
    ASSERT_EQ_FMT(0x8000, U16_SET_BIT(15), "0x%X");

    PASS();
}


TEST test_u32_set_bit(void)
{
    ASSERT_EQ_FMT(0x1, U32_SET_BIT(0), "0x%X");
    ASSERT_EQ_FMT(0x2, U32_SET_BIT(1), "0x%X");
    ASSERT_EQ_FMT(0x40000000U, U32_SET_BIT(30), "0x%X");
    ASSERT_EQ_FMT(0x80000000U, U32_SET_BIT(31), "0x%X");

    PASS();
}


TEST test_u64_set_bit(void)
{
    ASSERT_EQ_FMT(0x1UL, U64_SET_BIT(0), "0x%lX");
    ASSERT_EQ_FMT(0x2UL, U64_SET_BIT(1), "0x%lX");
    ASSERT_EQ_FMT(0x4000000000000000UL, U64_SET_BIT(62), "0x%lX");
    ASSERT_EQ_FMT(0x8000000000000000UL, U64_SET_BIT(63), "0x%lX");

    PASS();
}


TEST test_reverse_uint8_bits(void)
{
    ASSERT_EQ_FMT(0x00, reverse_uint8_bits(0x00), "0x%X");
    ASSERT_EQ_FMT(0x80, reverse_uint8_bits(0x01), "0x%X");
    ASSERT_EQ_FMT(0xC0, reverse_uint8_bits(0x03), "0x%X");
    ASSERT_EQ_FMT(0xFF, reverse_uint8_bits(0xFF), "0x%X");

    PASS();
}


TEST test_reverse_uint32_bits(void)
{
    ASSERT_EQ_FMT(0x00, reverse_uint32_bits(0x00), "0x%X");
    ASSERT_EQ_FMT(0x80000000, reverse_uint32_bits(0x00000001), "0x%X");
    ASSERT_EQ_FMT(0xC0000000, reverse_uint32_bits(0x00000003), "0x%X");
    ASSERT_EQ_FMT(0xFFFFFFFF, reverse_uint32_bits(0xFFFFFFFF), "0x%X");

    PASS();
}


GREATEST_MAIN_DEFS();


int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TEST(test_set_bit);
    RUN_TEST(test_is_power_of_two);
    RUN_TEST(test_u16_set_bit);
    RUN_TEST(test_u32_set_bit);
    RUN_TEST(test_u64_set_bit);
    RUN_TEST(test_reverse_uint8_bits);
    RUN_TEST(test_reverse_uint32_bits);

    GREATEST_MAIN_END();
}
