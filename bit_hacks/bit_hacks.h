/** \file
 * Helper functions for bit twiddling.
 */


#ifndef BIT_HACKS_H
#define BIT_HACKS_H


#include <stdint.h>


/**
 * Create an expression of type uint16_t with a single bit set at bit position X
 * (with 0 = least significant bit).
 */
#define SET_BIT_U16(x) ((uint16_t)1U << (x))


/**
 * Create an expression of type uint32_t with a single bit set at bit position X
 * (with 0 = least significant bit).
 */
#define SET_BIT_U32(x) ((uint32_t)1 << (x))


/**
 * Create an expression of type uint64_t with a single bit set at bit position X
 * (with 0 = least significant bit).
 */
#define SET_BIT_U64(x) ((uint64_t)1 << (x))


/**
 * Given an non-negative integer, return true if its value is a power of two.
 */
#define IS_POWER_OF_TWO(x) ((x)) && (((x) & ((x) - 1)) == 0)


/**
 * Reverse order of bits of an uint8_t value.
 */
static inline uint8_t reverse_bits_u8(uint8_t value)
{
    uint8_t reversed = 0;

    for (uint_fast8_t i = 0; i < 8; ++i)
    {
        if (value & (1U << i))
        {
            reversed |= (1U << (7U - i));
        }
    }

    return reversed;
}


/**
 * Reverse order of bits of an uint32_t value.
 */
static inline uint32_t reverse_bits_u32(uint32_t value)
{
    uint32_t reversed = 0;

    for (uint_fast8_t i = 0; i < 32; ++i)
    {
        if (value & (1U << i))
        {
            reversed |= (1U << (31U - i));
        }
    }

    return reversed;
}


/**
 * Count number of 1 bits.
 */
static inline uint_least8_t bits_set_count(uint32_t value)
{
    uint_least8_t bits = 0;

    for (int i = 0; i < 32; ++i)
    {
        if (value & 1)
        {
            ++bits;
        }

        value = value >> 1;
    }

    return bits;
}


#endif
