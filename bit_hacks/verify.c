#include "bit_hacks.h"
#include <stdint.h>

uint32_t nondet_uint32(void);
uint8_t nondet_uint8(void);

void verificatin_case_is_power_of_two(void)
{
    int result = IS_POWER_OF_TWO(nondet_uint32());
}

void verificatin_case_u16_set_bit(void)
{
    uint8_t bit = nondet_uint8();
    __CPROVER_assume(bit < 16);
    uint16_t ui16 = U16_SET_BIT(bit);
}

void verificatin_case_u32_set_bit(void)
{
    uint8_t bit = nondet_uint8();
    __CPROVER_assume(bit < 32);
    uint32_t ui32 = U32_SET_BIT(bit);
}

void verificatin_case_u64_set_bit(void)
{
    uint8_t bit = nondet_uint8();
    __CPROVER_assume(bit < 64);
    uint64_t ui64 = U64_SET_BIT(bit);
}

void verificatin_case_reverse_uint8_bits(void)
{
    uint32_t input = nondet_uint8();
    uint32_t output = reverse_uint8_bits(input);
}

void verificatin_case_reverse_uint32_bits(void)
{
    uint32_t input = nondet_uint32();
    uint32_t output = reverse_uint32_bits(input);
}

int main(void)
{
    verificatin_case_is_power_of_two();
    verificatin_case_u16_set_bit();
    verificatin_case_u32_set_bit();
    verificatin_case_u64_set_bit();
    verificatin_case_reverse_uint8_bits();
    verificatin_case_reverse_uint32_bits();

    return 0;
}
