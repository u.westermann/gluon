/** \file
 * Insertion sort.
 */

#ifndef INSERTION_SORT_H
#define INSERTION_SORT_H

/** CONFIGURATION: To use a different data type, modify it here or define it before including the header file. */
#ifndef INSERTION_SORT_TYPE
#define INSERTION_SORT_TYPE uint32_t
#endif

/** CONFIGURATION: Define for header only library, undefine for standalone module. */
//#define INSERTION_SORT_HEADER_ONLY

#ifdef INSERTION_SORT_HEADER_ONLY
#define INSERTION_SORT_SPEC static inline
#else
#define INSERTION_SORT_SPEC
#endif

#include <stdint.h>

INSERTION_SORT_SPEC void insertion_sort(INSERTION_SORT_TYPE* data, uint32_t length);

#ifdef INSERTION_SORT_HEADER_ONLY
#include "insertion_sort.c"
#endif

#endif
