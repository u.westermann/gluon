CC = gcc

TARGET = run-test

SRCS = insertion_sort.c
TEST_SRCS = $(SRCS) test.c
TEST_OBJS = $(addsuffix .o, $(basename $(TEST_SRCS)))
TEST_DEPS = $(TEST_OBJS:.o=.d)

INC_DIRS = $(addprefix -I, . ../external/greatest/)
CPPFLAGS = $(INC_DIRS) -D INSERTION_SORT_TYPE=int64_t -MMD -MP
CFLAGS = -g -std=c99 -Wall

.PHONY: test
test: $(TARGET)
	./$(TARGET)

$(TARGET): $(TEST_OBJS)
	$(CC) $(LDFLAGS) $(TEST_OBJS) -o $@ $(LDLIBS)

-include $(TEST_DEPS)

.PHONY: tag
tag:
	ctags -R .

.PHONY: clean
clean:
	$(RM) $(TARGET) $(TEST_OBJS) $(TEST_DEPS) tags

.PHONY: verify
verify:
	cbmc --c99 --stop-on-fail --unwinding-assertions --beautify --trace --bounds-check --pointer-check --memory-leak-check --div-by-zero-check --signed-overflow-check --pointer-overflow-check --conversion-check --undefined-shift-check --float-overflow-check --nan-check verify.c $(SRCS)

.PHONY: check
check:
	cppcheck --std=c99 --enable=all $(SRCS)
