#ifndef INSERTION_SORT_HEADER_ONLY
#include "insertion_sort.h"
#endif

#include <assert.h>
#include <stddef.h>

INSERTION_SORT_SPEC void insertion_sort(INSERTION_SORT_TYPE* data, uint32_t length)
{
    assert(data != NULL);

    for (uint_fast32_t i = 1; i < length; ++i)
    {
        for (uint_fast32_t j = i; (j > 0) && (data[j - 1] > data[j]); --j)
        {
            INSERTION_SORT_TYPE tmp = data[j - 1];
            data[j - 1] = data[j];
            data[j] = tmp;
        }
    }
}
