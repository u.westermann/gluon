#include "greatest.h"
#include "insertion_sort.h"
#include <stdint.h>

TEST test_insertion_sort(void)
{
    int64_t data[] = {4, 65, 2, -31, 0, 99, 2, 83, 782, 1};
    const size_t length = sizeof(data) / sizeof(int64_t);

    insertion_sort(data, length);

    ASSERT_EQ_FMT(-31L, data[0], "%ld");
    ASSERT_EQ_FMT(0L, data[1], "%ld");
    ASSERT_EQ_FMT(1L, data[2], "%ld");
    ASSERT_EQ_FMT(2L, data[3], "%ld");
    ASSERT_EQ_FMT(2L, data[4], "%ld");
    ASSERT_EQ_FMT(4L, data[5], "%ld");
    ASSERT_EQ_FMT(65L, data[6], "%ld");
    ASSERT_EQ_FMT(83L, data[7], "%ld");
    ASSERT_EQ_FMT(99L, data[8], "%ld");
    ASSERT_EQ_FMT(782L, data[9], "%ld");

    PASS();
}

TEST test_insertion_sort_check_buffer_overflow(void)
{
    int64_t data[] = {4, 65, 2, -31, 0, 99, 2, 83, 782, 1};
    const size_t length = sizeof(data) / sizeof(int64_t);

    insertion_sort(&data[1], length - 2);

    ASSERT_EQ_FMT(4L, data[0], "%ld");
    ASSERT_EQ_FMT(-31L, data[1], "%ld");
    ASSERT_EQ_FMT(0L, data[2], "%ld");
    ASSERT_EQ_FMT(2L, data[3], "%ld");
    ASSERT_EQ_FMT(2L, data[4], "%ld");
    ASSERT_EQ_FMT(65L, data[5], "%ld");
    ASSERT_EQ_FMT(83L, data[6], "%ld");
    ASSERT_EQ_FMT(99L, data[7], "%ld");
    ASSERT_EQ_FMT(782L, data[8], "%ld");
    ASSERT_EQ_FMT(1L, data[9], "%ld");

    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TEST(test_insertion_sort);
    RUN_TEST(test_insertion_sort_check_buffer_overflow);

    GREATEST_MAIN_END();
}
