var searchData=
[
  ['u16_5fset_5fbit',['U16_SET_BIT',['../bit__hacks_8h.html#a5d5cc979bba2c75551f4a97a33b902ef',1,'bit_hacks.h']]],
  ['u32_5fset_5fbit',['U32_SET_BIT',['../bit__hacks_8h.html#a8b3535ebdbe2e88b533c69e527586e1b',1,'bit_hacks.h']]],
  ['u64_5fset_5fbit',['U64_SET_BIT',['../bit__hacks_8h.html#ad2d98dd947a531e52070a4483b20674c',1,'bit_hacks.h']]],
  ['uint16_5fto_5fint16_5floss',['UINT16_TO_INT16_LOSS',['../checks_8h.html#a09875c25cc5338183a477eb92f5d0e2a',1,'checks.h']]],
  ['uint16_5fto_5fuint8_5floss',['UINT16_TO_UINT8_LOSS',['../checks_8h.html#ae2356ecefba09f816cb8912fba37cf58',1,'checks.h']]],
  ['uint32_5fto_5fint32_5floss',['UINT32_TO_INT32_LOSS',['../checks_8h.html#a6d17f4f306188e5ec0d4bb7077b1bd5d',1,'checks.h']]],
  ['uint32_5fto_5fuint16_5floss',['UINT32_TO_UINT16_LOSS',['../checks_8h.html#a0d2692aba22265de50090e52cad6d89c',1,'checks.h']]],
  ['uint64_5fto_5fint64_5floss',['UINT64_TO_INT64_LOSS',['../checks_8h.html#a800ffb9294adf32e0544a9af9916c50b',1,'checks.h']]],
  ['uint64_5fto_5fuint32_5floss',['UINT64_TO_UINT32_LOSS',['../checks_8h.html#a319eab81855471921c64c9811568d95e',1,'checks.h']]],
  ['uint8_5fto_5fint8_5floss',['UINT8_TO_INT8_LOSS',['../checks_8h.html#a0a083dcd88d02f6161c6c3639a2ae630',1,'checks.h']]]
];
