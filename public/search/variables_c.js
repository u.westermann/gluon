var searchData=
[
  ['name_5fbuf',['name_buf',['../structgreatest__run__info.html#a037b1cb46cd02ba3ef5f6ddddfffb7a9',1,'greatest_run_info']]],
  ['name_5fsuffix',['name_suffix',['../structgreatest__run__info.html#ad9670bbf1608bc2fa32fbea667944b8f',1,'greatest_run_info']]],
  ['next',['next',['../structchunk.html#a1c727c737dc665a5418ef296b9a80762',1,'chunk::next()'],['../structlist__node.html#a0656e14b4a9d75f88dae0493990af83b',1,'list_node::next()']]],
  ['next_5fstuff_5findex',['next_stuff_index',['../structcobs__decode.html#a66b221051ad55f3d31c76c1e5b895298',1,'cobs_decode']]],
  ['node',['node',['../structtest__node.html#a06150773a7d4c4780fc577ccc02cb6e6',1,'test_node']]],
  ['num',['num',['../structlist__mem.html#a140d449f4eaaa9afc56f07abb4922597',1,'list_mem']]],
  ['num_5frounds',['num_rounds',['../structxtea__cbc__t.html#af486415b60e9011066054bfb89d80ece',1,'xtea_cbc_t']]],
  ['number_5fof_5fblocks',['number_of_blocks',['../structcobs__encode.html#a920587de08a5ab22a362f687f94154e2',1,'cobs_encode']]]
];
