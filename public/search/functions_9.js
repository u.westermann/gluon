var searchData=
[
  ['rb_5fget_5fcount',['rb_get_count',['../ring__buffer_8c.html#acca88893dc01c26516b20e5a3c60e88a',1,'rb_get_count(ring_buffer_t *rb):&#160;ring_buffer.c'],['../ring__buffer_8h.html#acca88893dc01c26516b20e5a3c60e88a',1,'rb_get_count(ring_buffer_t *rb):&#160;ring_buffer.c']]],
  ['rb_5finit',['rb_init',['../ring__buffer_8c.html#a653a84309e2deed3f51accbc2bc994f8',1,'rb_init(ring_buffer_t *rb, RB_TYPE *buffer, uint32_t length):&#160;ring_buffer.c'],['../ring__buffer_8h.html#a653a84309e2deed3f51accbc2bc994f8',1,'rb_init(ring_buffer_t *rb, RB_TYPE *buffer, uint32_t length):&#160;ring_buffer.c']]],
  ['rb_5fread',['rb_read',['../ring__buffer_8c.html#a0789028e3edc48a3f93cb1f2a2639007',1,'rb_read(ring_buffer_t *rb, RB_TYPE *value):&#160;ring_buffer.c'],['../ring__buffer_8h.html#a0789028e3edc48a3f93cb1f2a2639007',1,'rb_read(ring_buffer_t *rb, RB_TYPE *value):&#160;ring_buffer.c']]],
  ['rb_5fwrite',['rb_write',['../ring__buffer_8c.html#a9d433447b1686d4eeec3d6aa34a65e3c',1,'rb_write(ring_buffer_t *rb, RB_TYPE value):&#160;ring_buffer.c'],['../ring__buffer_8h.html#a9d433447b1686d4eeec3d6aa34a65e3c',1,'rb_write(ring_buffer_t *rb, RB_TYPE value):&#160;ring_buffer.c']]],
  ['reverse_5fuint32_5fbits',['reverse_uint32_bits',['../bit__hacks_8c.html#afcfa1fe17a4acc518ca5065fb32d60dc',1,'reverse_uint32_bits(uint32_t value):&#160;bit_hacks.c'],['../bit__hacks_8h.html#afcfa1fe17a4acc518ca5065fb32d60dc',1,'reverse_uint32_bits(uint32_t value):&#160;bit_hacks.c']]],
  ['reverse_5fuint8_5fbits',['reverse_uint8_bits',['../bit__hacks_8c.html#a9e5edb66844fb07c735dab608d4e775f',1,'reverse_uint8_bits(uint8_t value):&#160;bit_hacks.c'],['../bit__hacks_8h.html#a9e5edb66844fb07c735dab608d4e775f',1,'reverse_uint8_bits(uint8_t value):&#160;bit_hacks.c']]]
];
