var searchData=
[
  ['pid_5finit',['pid_init',['../pid__controller_8c.html#a037cec7ec9c7aadfcb9274df135f75d0',1,'pid_init(pid_t *pid, float kp, float ki, float kd, float min_output_limit, float max_output_limit, float acceptable_error):&#160;pid_controller.c'],['../pid__controller_8h.html#a037cec7ec9c7aadfcb9274df135f75d0',1,'pid_init(pid_t *pid, float kp, float ki, float kd, float min_output_limit, float max_output_limit, float acceptable_error):&#160;pid_controller.c']]],
  ['pid_5fprocess',['pid_process',['../pid__controller_8c.html#a313213ef0bfa85b113916dab2e55f831',1,'pid_process(pid_t *pid, float measured_process_value, float dt):&#160;pid_controller.c'],['../pid__controller_8h.html#abff8c612af4d5c17b2f7141e7b557db2',1,'pid_process(pid_t *pid, float process_value, float dt):&#160;pid_controller.c']]],
  ['pid_5fset_5fpoint',['pid_set_point',['../pid__controller_8c.html#aabd48cceaa1e175ce62c4297dd97c39e',1,'pid_set_point(pid_t *pid, float set_point):&#160;pid_controller.c'],['../pid__controller_8h.html#aabd48cceaa1e175ce62c4297dd97c39e',1,'pid_set_point(pid_t *pid, float set_point):&#160;pid_controller.c']]]
];
