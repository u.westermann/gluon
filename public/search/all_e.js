var searchData=
[
  ['random_5forder',['random_order',['../structgreatest__prng.html#a56fb193b3c290daf7630a29b91a7a117',1,'greatest_prng']]],
  ['rb_5fget_5fcount',['rb_get_count',['../ring__buffer_8c.html#acca88893dc01c26516b20e5a3c60e88a',1,'rb_get_count(ring_buffer_t *rb):&#160;ring_buffer.c'],['../ring__buffer_8h.html#acca88893dc01c26516b20e5a3c60e88a',1,'rb_get_count(ring_buffer_t *rb):&#160;ring_buffer.c']]],
  ['rb_5finit',['rb_init',['../ring__buffer_8c.html#a653a84309e2deed3f51accbc2bc994f8',1,'rb_init(ring_buffer_t *rb, RB_TYPE *buffer, uint32_t length):&#160;ring_buffer.c'],['../ring__buffer_8h.html#a653a84309e2deed3f51accbc2bc994f8',1,'rb_init(ring_buffer_t *rb, RB_TYPE *buffer, uint32_t length):&#160;ring_buffer.c']]],
  ['rb_5fread',['rb_read',['../ring__buffer_8c.html#a0789028e3edc48a3f93cb1f2a2639007',1,'rb_read(ring_buffer_t *rb, RB_TYPE *value):&#160;ring_buffer.c'],['../ring__buffer_8h.html#a0789028e3edc48a3f93cb1f2a2639007',1,'rb_read(ring_buffer_t *rb, RB_TYPE *value):&#160;ring_buffer.c']]],
  ['rb_5ftype',['RB_TYPE',['../ring__buffer_8h.html#af72901092687a69d9997872b8b25df60',1,'ring_buffer.h']]],
  ['rb_5fwrite',['rb_write',['../ring__buffer_8c.html#a9d433447b1686d4eeec3d6aa34a65e3c',1,'rb_write(ring_buffer_t *rb, RB_TYPE value):&#160;ring_buffer.c'],['../ring__buffer_8h.html#a9d433447b1686d4eeec3d6aa34a65e3c',1,'rb_write(ring_buffer_t *rb, RB_TYPE value):&#160;ring_buffer.c']]],
  ['read',['read',['../structring__buffer.html#a0a71cf941cf2509857d61e1443ad8eaa',1,'ring_buffer']]],
  ['readme_2emd',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['reverse_5fuint32_5fbits',['reverse_uint32_bits',['../bit__hacks_8c.html#afcfa1fe17a4acc518ca5065fb32d60dc',1,'reverse_uint32_bits(uint32_t value):&#160;bit_hacks.c'],['../bit__hacks_8h.html#afcfa1fe17a4acc518ca5065fb32d60dc',1,'reverse_uint32_bits(uint32_t value):&#160;bit_hacks.c']]],
  ['reverse_5fuint8_5fbits',['reverse_uint8_bits',['../bit__hacks_8c.html#a9e5edb66844fb07c735dab608d4e775f',1,'reverse_uint8_bits(uint8_t value):&#160;bit_hacks.c'],['../bit__hacks_8h.html#a9e5edb66844fb07c735dab608d4e775f',1,'reverse_uint8_bits(uint8_t value):&#160;bit_hacks.c']]],
  ['ring_5fbuffer',['ring_buffer',['../structring__buffer.html',1,'']]],
  ['ring_5fbuffer_2ec',['ring_buffer.c',['../ring__buffer_8c.html',1,'']]],
  ['ring_5fbuffer_2eh',['ring_buffer.h',['../ring__buffer_8h.html',1,'']]],
  ['ring_5fbuffer_5fspec',['RING_BUFFER_SPEC',['../ring__buffer_8h.html#a0c18775e0bc3e273e024d969e91d63a1',1,'ring_buffer.h']]],
  ['ring_5fbuffer_5ft',['ring_buffer_t',['../ring__buffer_8h.html#a963a1a53bdd33928539cf4fdea3f40f9',1,'ring_buffer.h']]],
  ['run_5fsuite',['RUN_SUITE',['../byte__stack_2greatest_8h.html#a24d81276d45523c78c7432648cba646c',1,'RUN_SUITE():&#160;greatest.h'],['../checks_2greatest_8h.html#a24d81276d45523c78c7432648cba646c',1,'RUN_SUITE():&#160;greatest.h'],['../list_2greatest_8h.html#a24d81276d45523c78c7432648cba646c',1,'RUN_SUITE():&#160;greatest.h']]],
  ['run_5ftest',['RUN_TEST',['../byte__stack_2greatest_8h.html#a338adb9f062605032a20b13dbfe172e0',1,'RUN_TEST():&#160;greatest.h'],['../checks_2greatest_8h.html#a338adb9f062605032a20b13dbfe172e0',1,'RUN_TEST():&#160;greatest.h'],['../list_2greatest_8h.html#a338adb9f062605032a20b13dbfe172e0',1,'RUN_TEST():&#160;greatest.h']]],
  ['run_5ftest1',['RUN_TEST1',['../byte__stack_2greatest_8h.html#a6860f4aafe86be1ea0afdc8bf23a0827',1,'RUN_TEST1():&#160;greatest.h'],['../checks_2greatest_8h.html#a6860f4aafe86be1ea0afdc8bf23a0827',1,'RUN_TEST1():&#160;greatest.h'],['../list_2greatest_8h.html#a6860f4aafe86be1ea0afdc8bf23a0827',1,'RUN_TEST1():&#160;greatest.h']]]
];
