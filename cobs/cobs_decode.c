#include "cobs_decode.h"
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

void cobs_decode_init(cobs_decode_t* cobs, uint8_t* data, uint_fast32_t length)
{
    assert(cobs != NULL);
    assert(data != NULL);

    cobs->data = data;
    cobs->length = length;
    cobs->index = 0;
    cobs->next_stuff_index = 0;
}

bool is_end(cobs_decode_t* cobs)
{
    return cobs->index >= cobs->length;
}

bool is_overhead_byte(cobs_decode_t* cobs)
{
    return cobs->index % 255 == 0;
}

bool is_stuffed_byte(cobs_decode_t* cobs)
{
    return cobs->index == cobs->next_stuff_index;
}

int_fast16_t cobs_decode_get_next(cobs_decode_t* cobs)
{
    assert(cobs != NULL);
    assert(cobs->data != NULL);

    if (is_end(cobs))
    {
        return -1;
    }

    if (is_overhead_byte(cobs))
    {
        cobs->next_stuff_index = cobs->data[cobs->index] + cobs->index;
        cobs->index++;
    }

    uint_fast8_t byte;

    if (is_stuffed_byte(cobs))
    {
        cobs->next_stuff_index = cobs->index + cobs->data[cobs->index];
        byte = 0;
    }
    else
    {
        byte = cobs->data[cobs->index];
    }

    cobs->index++;

    return byte;
}
