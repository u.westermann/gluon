/** \file
 * Consistent overhead byte stuffing encoder.
 *
 * https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing
 */

#ifndef COBS_ENCODE_H
#define COBS_ENCODE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdint.h>

    typedef struct cobs_encode
    {
        uint8_t* data;
        uint_fast32_t length;
        uint_fast32_t number_of_blocks;
        uint_fast32_t block_counter;
        uint_fast8_t current_block_index;
        uint_fast8_t current_block_length;
        uint8_t block[256];
    } cobs_encode_t;

    void cobs_encode_init(cobs_encode_t* cobs, uint8_t* data,
                          uint_fast32_t length);
    uint_fast8_t cobs_encode_get_next(cobs_encode_t* cobs);

#ifdef __cplusplus
}
#endif

#endif
