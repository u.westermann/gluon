/** \file
 * Consistent overhead byte stuffing decoder.
 *
 * https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing
 */

#ifndef COBS_DECODE_H
#define COBS_DECODE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdint.h>

    typedef struct cobs_decode
    {
        uint8_t* data;
        uint_fast32_t length;
        uint_fast32_t index;
        uint_fast32_t next_stuff_index;
    } cobs_decode_t;

    void cobs_decode_init(cobs_decode_t* cobs, uint8_t* data,
                          uint_fast32_t length);
    int_fast16_t cobs_decode_get_next(cobs_decode_t* cobs);

#ifdef __cplusplus
}
#endif

#endif
