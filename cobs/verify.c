#include "cobs_decode.h"
#include "cobs_encode.h"
#include <assert.h>
#include <stdint.h>

uint32_t nondet_uint32(void);

void verification_case_encode_decode(void)
{
    uint8_t original[3];
    uint8_t encoded[sizeof(original) + 1];
    uint8_t decoded[sizeof(original)];

    cobs_encode_t encoder;
    cobs_decode_t decoder;

    cobs_encode_init(&encoder, original, sizeof(original));

    for (int i = 0; i < sizeof(encoded); ++i)
    {
        encoded[i] = cobs_encode_get_next(&encoder);
        assert(encoded[i] != 0);
    }
}

int main(void)
{
    verification_case_encode_decode();

    return 0;
}
