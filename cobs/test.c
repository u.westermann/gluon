#include "cobs_decode.h"
#include "cobs_encode.h"
#include "greatest.h"
#include <stdint.h>

TEST test_encode_decode(uint32_t count, bool zero_at_boundaries)
{
    uint8_t original[count];

    uint32_t number_of_overhead_bytes = count / 254;
    number_of_overhead_bytes += (count % 254) ? 1 : 0;

    uint8_t encoded[sizeof(original) + number_of_overhead_bytes];
    uint8_t decoded[sizeof(original)];

    cobs_encode_t encoder;
    cobs_decode_t decoder;

    memset(original, 0, sizeof(original));

    for (uint_fast32_t i = 0; i < count; i++)
    {
        if (i % 5 == 0)
        {
            original[i] = i % 256;
        }

        if (i % 8 == 0)
        {
            original[i] = i % 256;
        }

        if (i % 254 == 253 || i % 254 == 0)
        {
            if (zero_at_boundaries)
            {
                original[i] = 0;
            }
            else
            {
                original[i] = 42;
            }
        }
    }

    cobs_encode_init(&encoder, original, sizeof(original));

    for (int i = 0; i < sizeof(encoded); i++)
    {
        encoded[i] = cobs_encode_get_next(&encoder);
        ASSERT(encoded[i] != 0);
    }

    int_fast8_t end_of_packet = cobs_encode_get_next(&encoder);
    ASSERT(end_of_packet == 0);

    cobs_decode_init(&decoder, encoded, sizeof(encoded));

    for (int i = 0; i < sizeof(decoded); i++)
    {
        decoded[i] = cobs_decode_get_next(&decoder);
        ASSERT(decoded[i] == original[i]);
    }

    int_fast16_t no_more_bytes = cobs_decode_get_next(&decoder);
    ASSERT(no_more_bytes < 0);

    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TESTp(test_encode_decode, 0, false);
    RUN_TESTp(test_encode_decode, 0, true);
    RUN_TESTp(test_encode_decode, 1, false);
    RUN_TESTp(test_encode_decode, 1, true);
    RUN_TESTp(test_encode_decode, 2, false);
    RUN_TESTp(test_encode_decode, 2, true);
    RUN_TESTp(test_encode_decode, 253, false);
    RUN_TESTp(test_encode_decode, 253, true);
    RUN_TESTp(test_encode_decode, 254, false);
    RUN_TESTp(test_encode_decode, 254, true);
    RUN_TESTp(test_encode_decode, 255, false);
    RUN_TESTp(test_encode_decode, 255, true);
    RUN_TESTp(test_encode_decode, 507, false);
    RUN_TESTp(test_encode_decode, 507, true);
    RUN_TESTp(test_encode_decode, 508, false);
    RUN_TESTp(test_encode_decode, 508, true);
    RUN_TESTp(test_encode_decode, 509, false);
    RUN_TESTp(test_encode_decode, 509, true);
    RUN_TESTp(test_encode_decode, 510, false);
    RUN_TESTp(test_encode_decode, 510, true);

    GREATEST_MAIN_END();
}
