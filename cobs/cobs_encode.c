#include "cobs_encode.h"
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

static uint_fast8_t calc_current_block_length(cobs_encode_t* cobs);
static void encode_block(cobs_encode_t* cobs);
static bool is_end(cobs_encode_t* cobs);
static bool is_block_start(cobs_encode_t* cobs);
static bool is_block_end(cobs_encode_t* cobs);
static void prepare_next_block(cobs_encode_t* cobs);
static uint_fast8_t get_next_encoded_byte(cobs_encode_t* cobs);

void cobs_encode_init(cobs_encode_t* cobs, uint8_t* data, uint_fast32_t length)
{
    assert(cobs != NULL);
    assert(data != NULL);

    cobs->data = data;
    cobs->length = length;

    cobs->number_of_blocks = length / 254;

    if (length % 254)
    {
        cobs->number_of_blocks++;
    }

    cobs->block_counter = 0;
    cobs->current_block_index = 0;
}

uint_fast8_t cobs_encode_get_next(cobs_encode_t* cobs)
{
    assert(cobs != NULL);
    assert(cobs->data != NULL);

    if (is_end(cobs))
    {
        return 0;
    }

    if (is_block_start(cobs))
    {
        encode_block(cobs);
    }

    uint_fast8_t byte = get_next_encoded_byte(cobs);

    if (is_block_end(cobs))
    {
        prepare_next_block(cobs);
    }

    return byte;
}

static uint_fast8_t calc_current_block_length(cobs_encode_t* cobs)
{
    uint_fast32_t remaining = cobs->length - cobs->block_counter * 254;

    if (remaining > 254)
    {
        return 254;
    }

    return remaining;
}

static void encode_block(cobs_encode_t* cobs)
{
    cobs->current_block_length = calc_current_block_length(cobs);

    memcpy(cobs->block + 1, cobs->data + cobs->block_counter * 254,
           cobs->current_block_length);

    cobs->block[cobs->current_block_length + 1] = 0;

    uint_fast8_t previous_stuff_index = 0;

    for (uint_fast16_t i = 1; i <= cobs->current_block_length + 1; i++)
    {
        if (cobs->block[i] == 0)
        {
            cobs->block[previous_stuff_index] = i - previous_stuff_index;
            previous_stuff_index = i;
        }
    }
}

static bool is_end(cobs_encode_t* cobs)
{
    return cobs->block_counter >= cobs->number_of_blocks;
}

static bool is_block_start(cobs_encode_t* cobs)
{
    return cobs->current_block_index == 0;
}

static bool is_block_end(cobs_encode_t* cobs)
{
    return cobs->current_block_index > cobs->current_block_length;
}

static void prepare_next_block(cobs_encode_t* cobs)
{
    cobs->current_block_index = 0;
    cobs->block_counter++;
}

static uint_fast8_t get_next_encoded_byte(cobs_encode_t* cobs)
{
    return cobs->block[cobs->current_block_index++];
}
