#!/usr/bin/env bash

set -eux

IFS=$'\n'

for folder in $(find . -type d); do 
    if [ -f "$folder"/verify.c ]; then
        make -C "$folder" verify
    fi
done

echo SUCCESS
