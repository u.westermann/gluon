#ifndef SELECTION_SORT_HEADER_ONLY
#include "selection_sort.h"
#endif

#include <assert.h>
#include <stddef.h>

SELECTION_SORT_SPEC void selection_sort(SELECTION_SORT_TYPE* data, uint32_t length, uint32_t stop_after_n_sorted)
{
    assert(data != NULL);
    assert(stop_after_n_sorted <= length);

    for (uint_fast32_t i = 0; i < stop_after_n_sorted; ++i)
    {
        uint_fast32_t minimum = i;

        for (uint_fast32_t j = i + 1; j < length; ++j)
        {
            if (data[j] < data[minimum])
            {
                minimum = j;
            }
        }

        if (minimum != i)
        {
            SELECTION_SORT_TYPE tmp = data[i];
            data[i] = data[minimum];
            data[minimum] = tmp;
        }
    }
}
