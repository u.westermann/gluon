#include "greatest.h"
#include "selection_sort.h"
#include <stdint.h>

TEST test_selection_sort(void)
{
    int16_t data[] = {4, 65, 2, -31, 0, 99, 2, 83, 782, 1};
    const size_t length = sizeof(data) / sizeof(int16_t);

    selection_sort(data, length, length - 1);

    ASSERT_EQ_FMT(-31, data[0], "%d");
    ASSERT_EQ_FMT(0, data[1], "%d");
    ASSERT_EQ_FMT(1, data[2], "%d");
    ASSERT_EQ_FMT(2, data[3], "%d");
    ASSERT_EQ_FMT(2, data[4], "%d");
    ASSERT_EQ_FMT(4, data[5], "%d");
    ASSERT_EQ_FMT(65, data[6], "%d");
    ASSERT_EQ_FMT(83, data[7], "%d");
    ASSERT_EQ_FMT(99, data[8], "%d");
    ASSERT_EQ_FMT(782, data[9], "%d");

    PASS();
}

TEST test_selection_sort_check_buffer_overflow(void)
{
    int16_t data[] = {4, 65, 2, -31, 0, 99, 2, 83, 782, 1};
    const size_t length = sizeof(data) / sizeof(int16_t);

    selection_sort(&data[1], length - 2, length - 3);

    ASSERT_EQ_FMT(4, data[0], "%d");
    ASSERT_EQ_FMT(-31, data[1], "%d");
    ASSERT_EQ_FMT(0, data[2], "%d");
    ASSERT_EQ_FMT(2, data[3], "%d");
    ASSERT_EQ_FMT(2, data[4], "%d");
    ASSERT_EQ_FMT(65, data[5], "%d");
    ASSERT_EQ_FMT(83, data[6], "%d");
    ASSERT_EQ_FMT(99, data[7], "%d");
    ASSERT_EQ_FMT(782, data[8], "%d");
    ASSERT_EQ_FMT(1, data[9], "%d");

    PASS();
}

TEST test_selection_sort_stop_after_half(void)
{
    int16_t data[] = {4, 65, 2, -31, 0, 99, 2, 83, 782, 1};
    const size_t length = sizeof(data) / sizeof(int16_t);

    selection_sort(data, length, length / 2);

    ASSERT_EQ_FMT(-31, data[0], "%d");
    ASSERT_EQ_FMT(0, data[1], "%d");
    ASSERT_EQ_FMT(1, data[2], "%d");
    ASSERT_EQ_FMT(2, data[3], "%d");
    ASSERT_EQ_FMT(2, data[4], "%d");

    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TEST(test_selection_sort);
    RUN_TEST(test_selection_sort_check_buffer_overflow);
    RUN_TEST(test_selection_sort_stop_after_half);

    GREATEST_MAIN_END();
}
