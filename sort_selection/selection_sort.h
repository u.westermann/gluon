/** \file
 * Selection sort.
 */

#ifndef SELECTION_SORT_H
#define SELECTION_SORT_H

/** CONFIGURATION: To use a different data type, modify it here or define it before including the header file. */
#ifndef SELECTION_SORT_TYPE
#define SELECTION_SORT_TYPE uint32_t
#endif

/** CONFIGURATION: Define for header only library, undefine for standalone module. */
//#define SELECTION_SORT_HEADER_ONLY

#ifdef SELECTION_SORT_HEADER_ONLY
#define SELECTION_SORT_SPEC static inline
#else
#define SELECTION_SORT_SPEC
#endif

#include <stdint.h>

SELECTION_SORT_SPEC void selection_sort(SELECTION_SORT_TYPE* data, uint32_t length, uint32_t stop_after_n_sorted);

#ifdef SELECTION_SORT_HEADER_ONLY
#include "selection_sort.c"
#endif

#endif
