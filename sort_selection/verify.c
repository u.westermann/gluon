#include "selection_sort.h"
#include <assert.h>

uint32_t nondet_uint32(void);

void verification_case_sort(void)
{
    const int LENGTH = 5;
    uint32_t data[LENGTH];

    for (int i = 0; i < LENGTH; ++i)
    {
        data[i] = nondet_uint32();
    }

    selection_sort(data, LENGTH, LENGTH - 1);

    for (int i = 0; i < LENGTH - 1; ++i)
    {
        assert(data[i] <= data[i + 1]);
    }
}

int main(void)
{
    verification_case_sort();

    return 0;
}
