#include "bit_hacks.h"
#include "crc32.h"
#include "greatest.h"
#include <stdint.h>

// CRC parameters and test vectors from http://reveng.sourceforge.net/crc-catalogue/

TEST test_crc32(void)
{
    uint8_t data[] = "123456789";
    uint8_t data_reversed[sizeof(data)];

    for (int i = 0; i < 9; ++i)
    {
        data_reversed[i] = reverse_uint8_bits(data[i]);
    }

    uint32_t crc;
    crc = crc32(0xFFFFFFFF, 0x04C11DB7, data_reversed, 9);
    crc = reverse_uint32_bits(crc);
    crc = ~crc;

    ASSERT_EQ_FMT(0xCBF43926, crc, "0x%X");

    PASS();
}

TEST test_crc32_bzip2(void)
{
    uint8_t data[] = "123456789";

    uint32_t crc;
    crc = crc32(0xFFFFFFFF, 0x04C11DB7, data, 9);
    crc = ~crc;

    ASSERT_EQ_FMT(0xFC891918, crc, "0x%X");

    PASS();
}

TEST test_crc32_c(void)
{
    uint8_t data[] = "123456789";
    uint8_t data_reversed[sizeof(data)];

    for (int i = 0; i < 9; ++i)
    {
        data_reversed[i] = reverse_uint8_bits(data[i]);
    }

    uint32_t crc;
    crc = crc32(0xFFFFFFFF, 0x1EDC6F41, data_reversed, 9);
    crc = reverse_uint32_bits(crc);
    crc = ~crc;

    ASSERT_EQ_FMT(0xE3069283, crc, "0x%X");

    PASS();
}

TEST test_crc32_posix(void)
{
    uint8_t data[] = "123456789";

    uint32_t crc;
    crc = crc32(0x00000000, 0x04C11DB7, data, 9);
    crc = ~crc;

    ASSERT_EQ_FMT(0x765E7680, crc, "0x%X");

    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TEST(test_crc32);
    RUN_TEST(test_crc32_bzip2);
    RUN_TEST(test_crc32_c);
    RUN_TEST(test_crc32_posix);

    GREATEST_MAIN_END();
}
