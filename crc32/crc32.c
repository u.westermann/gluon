#ifndef CRC32_HEADER_ONLY
#include "crc32.h"
#endif

#include <assert.h>
#include <stddef.h>

CRC32_SPEC uint32_t crc32(uint32_t init, uint32_t poly, const uint8_t data[],
                          uint_fast32_t length)
{
    uint32_t crc = init;

    for (uint_fast32_t i = 0; i < length; ++i)
    {
        uint_fast8_t byte = data[i];

        for (uint_fast8_t j = 0; j < 8; ++j)
        {
            uint_fast8_t poly_in_bit = (byte >> 7) ^ (crc >> 31);

            if (poly_in_bit)
            {
                // lowest bit of polynomial is always 1 and therefore will set
                // lowest crc bit
                crc = (crc << 1) ^ poly;
            }
            else
            {
                // xor with 0 poly_in_bit would not alter crc, so simply do a
                // shift
                crc = crc << 1;
            }

            byte = byte << 1;
        }
    }

    return crc;
}
