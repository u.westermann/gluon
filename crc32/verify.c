#include "crc32.h"

uint32_t nondet_uint32(void);

void verification_case_crc32(void)
{
    const int LENGTH = 5;
    uint8_t data[LENGTH];

    uint32_t crc = crc32(nondet_uint32(), nondet_uint32(), data, LENGTH);
}

int main(void)
{
    verification_case_crc32();

    return 0;
}
