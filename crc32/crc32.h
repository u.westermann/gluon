/** \file
 * 32 bit cyclic redundancy check.
 *
 * https://en.wikipedia.org/wiki/Cyclic_redundancy_check
 */

#ifndef CRC32_H
#define CRC32_H

/** CONFIGURATION: Define for header only library, undefine for standalone
 * module. */
//#define CRC32_HEADER_ONLY

#ifdef CRC32_HEADER_ONLY
#define CRC32_SPEC static inline
#else
#define CRC32_SPEC
#endif

#include <stdint.h>

CRC32_SPEC uint32_t crc32(uint32_t init, uint32_t poly, const uint8_t data[],
                          uint_fast32_t length);

#ifdef CRC32_HEADER_ONLY
#include "crc32.c"
#endif

#endif
