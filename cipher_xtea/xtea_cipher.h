/** \file
 * eXtended TEA (XTEA) is a block (symmetric key) cipher.
 * It takes a 128 bit key and encrypts/decrypts a block of 64 data bits.
 * Its implementation is simple and it can be considered sufficiently secure
 * for many applications (don't use it to secure nuclear launch codes).
 * To encode blocks longer than 64 bits, functions for the CBC (Cipher Block
 * Chaining) operational mode are included.
 *
 * Keep in mind that encryption only provides confidentiality and does not
 * ensure authenticity and integrity.
 *
 * https://en.wikipedia.org/wiki/XTEA
 * https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation
 */

#ifndef XTEA_CIPHER_H
#define XTEA_CIPHER_H

/** CONFIGURATION: Define for header only library, undefine for standalone
 * module. */
//#define XTEA_CIPHER_HEADER_ONLY

#ifdef XTEA_CIPHER_HEADER_ONLY
#define XTEA_CIPHER_SPEC static inline
#else
#define XTEA_CIPHER_SPEC
#endif

#include <stdint.h>

typedef struct
{
    uint32_t key[4];
    uint32_t num_rounds;
    uint32_t previous[2];
    int is_first;
} xtea_cbc_t;

XTEA_CIPHER_SPEC void xtea_cbc_init(xtea_cbc_t* xtea_cbc,
                                    uint32_t init_vector[2], uint32_t key[4],
                                    uint32_t num_rounds);
XTEA_CIPHER_SPEC void xtea_cbc_encrypt(xtea_cbc_t* xtea_cbc,
                                       const uint32_t clear[2],
                                       uint32_t encrypted[2]);
XTEA_CIPHER_SPEC void xtea_cbc_decrypt(xtea_cbc_t* xtea_cbc,
                                       const uint32_t encrypted[2],
                                       uint32_t clear[2]);
XTEA_CIPHER_SPEC void xtea_encrypt_block(const uint32_t clear[2],
                                         const uint32_t key[4],
                                         uint32_t encrypted[2],
                                         uint32_t num_rounds);
XTEA_CIPHER_SPEC void xtea_decrypt_block(const uint32_t encrypted[2],
                                         const uint32_t key[4],
                                         uint32_t clear[2],
                                         uint32_t num_rounds);

#ifdef XTEA_CIPHER_HEADER_ONLY
#include "xtea_cipher.c"
#endif

#endif
