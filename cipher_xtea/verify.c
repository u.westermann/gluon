#include "xtea_cipher.h"

uint32_t nondet_uint32(void);

void verification_case_encrypt_decrypt(void)
{
    uint32_t clear[2];
    uint32_t key[4];
    uint32_t encrypted[2];

    xtea_encrypt_block(clear, key, encrypted, 2);

    uint32_t decrypted[2];
    xtea_decrypt_block(encrypted, key, decrypted, 2);

    __CPROVER_assert(clear[0] == decrypted[0], "clear[0] == decrypted[0]");
    __CPROVER_assert(clear[1] == decrypted[1], "clear[1] == decrypted[1]");
}

int main(void)
{
    verification_case_encrypt_decrypt();

    return 0;
}
