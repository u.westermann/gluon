#ifndef XTEA_CIPHER_HEADER_ONLY
#include "xtea_cipher.h"
#endif

XTEA_CIPHER_SPEC void xtea_cbc_init(xtea_cbc_t* xtea_cbc,
                                    uint32_t init_vector[2], uint32_t key[4],
                                    uint32_t num_rounds)
{
    xtea_cbc->previous[0] = init_vector[0];
    xtea_cbc->previous[1] = init_vector[1];
    xtea_cbc->key[0] = key[0];
    xtea_cbc->key[1] = key[1];
    xtea_cbc->key[2] = key[2];
    xtea_cbc->key[3] = key[3];
    xtea_cbc->num_rounds = num_rounds;
}

XTEA_CIPHER_SPEC void xtea_cbc_encrypt(xtea_cbc_t* xtea_cbc,
                                       const uint32_t clear[2],
                                       uint32_t encrypted[2])
{
    uint32_t clear_xor[2];
    clear_xor[0] = clear[0] ^ xtea_cbc->previous[0];
    clear_xor[1] = clear[1] ^ xtea_cbc->previous[1];

    xtea_cbc->previous[0] = encrypted[0];
    xtea_cbc->previous[1] = encrypted[1];

    xtea_encrypt_block(clear_xor, xtea_cbc->key, encrypted,
                       xtea_cbc->num_rounds);
}

XTEA_CIPHER_SPEC void xtea_cbc_decrypt(xtea_cbc_t* xtea_cbc,
                                       const uint32_t encrypted[2],
                                       uint32_t clear[2])
{
    xtea_decrypt_block(encrypted, xtea_cbc->key, clear, xtea_cbc->num_rounds);

    clear[0] ^= xtea_cbc->previous[0];
    clear[1] ^= xtea_cbc->previous[1];

    xtea_cbc->previous[0] = encrypted[0];
    xtea_cbc->previous[1] = encrypted[1];
}

// based on orginal code from "Tea extensions" paper of Roger M. Needham and
// David J. Wheeler
XTEA_CIPHER_SPEC void xtea_encrypt_block(const uint32_t clear[2],
                                         const uint32_t key[4],
                                         uint32_t encrypted[2],
                                         uint32_t num_rounds)
{
    uint32_t y = clear[0];
    uint32_t z = clear[1];
    const uint32_t DELTA = 0x9e3779b9;
    uint32_t limit = DELTA * num_rounds;
    uint32_t sum = 0;

    while (sum != limit)
    {
        y += (((z << 4) ^ (z >> 5)) + z) ^ (sum + key[sum & 3]);
        sum += DELTA;
        z += (((y << 4) ^ (y >> 5)) + y) ^ (sum + key[(sum >> 11) & 3]);
    }

    encrypted[0] = y;
    encrypted[1] = z;
}

// based on orginal code from "Tea extensions" paper of Roger M. Needham and
// David J. Wheeler
XTEA_CIPHER_SPEC void xtea_decrypt_block(const uint32_t encrypted[2],
                                         const uint32_t key[4],
                                         uint32_t clear[2], uint32_t num_rounds)
{
    uint32_t y = encrypted[0];
    uint32_t z = encrypted[1];
    const uint32_t DELTA = 0x9e3779b9;
    uint32_t sum = DELTA * num_rounds;

    while (sum)
    {
        z -= (((y << 4) ^ (y >> 5)) + y) ^ (sum + key[(sum >> 11) & 3]);
        sum -= DELTA;
        y -= (((z << 4) ^ (z >> 5)) + z) ^ (sum + key[sum & 3]);
    }

    clear[0] = y;
    clear[1] = z;
}
