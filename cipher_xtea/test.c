#include "greatest.h"
#include "xtea_cipher.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

TEST test_xtea_encrypt_decrypt_block(void)
{
    uint32_t key[4] = {0x0001, 0x0002, 0x0003};
    uint32_t original[] = {42, 43};
    uint32_t encrypted[sizeof(original) / sizeof(uint32_t)];
    uint32_t decrypted[sizeof(original) / sizeof(uint32_t)];

    xtea_encrypt_block(original, key, encrypted, 32);
    xtea_decrypt_block(encrypted, key, decrypted, 32);

    for (uint_fast32_t i = 0; i < sizeof(original) / sizeof(uint32_t); i++)
    {
        ASSERT(original[i] == decrypted[i]);
    }

    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TEST(test_xtea_encrypt_decrypt_block);

    GREATEST_MAIN_END();
}
