#include "endian_check.h"
#include "greatest.h"
#include <stdint.h>

TEST test_big_endian(void)
{
    ASSERT_FALSE(is_big_endian());
    PASS();
}

TEST test_little_endian(void)
{
    ASSERT(is_little_endian());
    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_TEST(test_big_endian);
    RUN_TEST(test_little_endian);

    GREATEST_MAIN_END();
}
