#ifndef ENDIAN_CHECK_HEADER_ONLY
#include "endian_check.h"
#endif

#include <stdint.h>

ENDIAN_CHECK_SPEC int is_big_endian(void)
{
    union {
        uint16_t ui16;
        uint8_t ui8[2];
    } two_bytes;

    two_bytes.ui16 = 0x0102;

    return two_bytes.ui8[0] == 0x01;
}

ENDIAN_CHECK_SPEC int is_little_endian(void)
{
    return is_big_endian() == 0;
}
