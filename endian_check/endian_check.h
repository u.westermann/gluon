/** \file
 * Determine endianess of host machine.
 *
 * https://en.wikipedia.org/wiki/Endianness
 */

#ifndef ENDIAN_CHECK_H
#define ENDIAN_CHECK_H

/** CONFIGURATION: Define for header only library, undefine for standalone module. */
//#define ENDIAN_CHECK_HEADER_ONLY

#ifdef ENDIAN_CHECK_HEADER_ONLY
#define ENDIAN_CHECK_SPEC static inline
#else
#define ENDIAN_CHECK_SPEC
#endif

ENDIAN_CHECK_SPEC int is_big_endian(void);
ENDIAN_CHECK_SPEC int is_little_endian(void);

#ifdef ENDIAN_CHECK_HEADER_ONLY
#include "endian_check.c"
#endif

#endif
