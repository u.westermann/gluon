#include "queue.h"
#include "assert.h"
#include <stddef.h>


void rb_init(queue_t *queue, QUEUE_TYPE *buffer, uint32_t length) {
  assert(queue != NULL);
  assert(buffer != NULL);
  assert(length > 0);

  queue->buffer = buffer;
  queue->capacity = length;
  queue->count = 0;
  queue->read = 0;
  queue->write = 0;
}


int rb_read(queue_t *queue, QUEUE_TYPE *value) {
  assert(queue != NULL);
  assert(value != NULL);

  if (queue->count == 0) {
    return -1;
  }

  *value = queue->buffer[queue->read];
  queue->read = (queue->read + 1) % queue->capacity;
  queue->count--;

  return 0;
}


int rb_write(queue_t *queue, QUEUE_TYPE value) {
  assert(queue != NULL);

  if (queue->count >= queue->capacity) {
    return -1;
  }

  queue->buffer[queue->write] = value;
  queue->write = (queue->write + 1) % queue->capacity;
  ++queue->count;

  return 0;
}


uint32_t rb_get_count(queue_t *queue) {
  assert(queue != NULL);

  return queue->count;
}
