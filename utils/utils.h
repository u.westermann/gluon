/** \file
 * Checks for success of arithmetic operations.
 */


#ifndef CHECKS_H
#define CHECKS_H


#ifdef __cplusplus
extern "C"
{
#endif


#include <stdint.h>


/**
 * Get the size (number of elements) of an array.
 */
#define ARRAY_SIZE(array) (sizeof(array) / sizeof((array)[0]))


/**
 * Get the smaller of two values.
 */
#define MIN(x, y) ((x) < (y) ? (x) : (y))


/**
 * Get the greater of two values.
 */
#define MAX(x, y) ((x) > (y) ? (x) : (y))


/**
 * Set a single bit in x at position pos.
 */
#define SET_BIT(x, pos) ((1U << (pos)) | x)

/**
 * Given an non-negative integer, return true if its value is a power of two.
 */
#define IS_POWER_OF_TWO(x) (((x)) && (((x) & ((x) - 1)) == 0))


// addition a + b results in unsigned integer wrap
#define CHECK_OVERFLOW_ADD_UINT64(x, y) (UINT64_MAX - x < y)
#define CHECK_OVERFLOW_ADD_UINT32(x, y) (UINT32_MAX - x < y)
#define CHECK_OVERFLOW_ADD_UINT16(x, y) (UINT16_MAX - x < y)
#define CHECK_OVERFLOW_ADD_UINT8(x, y) (UINT8_MAX - x < y)

// subtraction a - b results in unsigned integer overflow
#define CHECK_OVERFLOW_SUB_UINT(a, b) (a < b)

// cast of a to next smaller type would lose data
#define CHECK_LOSS_UINT64_TO_UINT32(x) (x > UINT32_MAX)
#define CHECK_LOSS_UINT32_TO_UINT16(x) (x > UINT16_MAX)
#define CHECK_LOSS_UINT16_TO_UINT8(x) (x > UINT8_MAX)

// cast from unsigned to signed would would lose data
#define CHECK_LOSS_UINT64_TO_INT64(x) (x > INT64_MAX)
#define CHECK_LOSS_UINT32_TO_INT32(x) (x > INT32_MAX)
#define CHECK_LOSS_UINT16_TO_INT16(x) (x > INT16_MAX)
#define CHECK_LOSS_UINT8_TO_INT8(x) (x > INT8_MAX)


#ifdef __cplusplus
}
#endif


#endif
