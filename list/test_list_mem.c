#include "greatest.h"
#include "list_mem.h"


struct test_node
{
    struct list_node node;
    int value;
};


static int action_print(void* node)
{
    struct test_node* n = node;

    printf("\naddr: %p, next: %p, value: %d", (void*)n,
           (void*)((struct list_node*)node)->next, n->value);

    return 0;
}


TEST test_list_mem()
{
    list_mem_t self;
    struct test_node node_mem[3] = {{{.next = (void*)0xDEAD}, .value = 1}, {{.next = (void*)0xDEAD}, .value = 2}, {{.next = (void*)0xDEAD}, .value = 3}};

    list_mem_init(&self, node_mem, 3, sizeof(struct test_node));

    list_basic_foreach(self.first, action_print);

    struct test_node* node1 = list_mem_alloc(&self);
    ASSERT(node1);

    struct test_node* node2 = list_mem_alloc(&self);
    ASSERT(node2);

    struct test_node* node3 = list_mem_alloc(&self);
    ASSERT(node3);

    ASSERT(list_mem_alloc(&self) == NULL);

    PASS();
}


SUITE(list_mem)
{
    RUN_TEST(test_list_mem);
}
