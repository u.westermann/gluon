/** \file
 * Memory management based on singly linked list.
 */


#ifndef list_mem_h
#define list_mem_h


#ifdef __cplusplus
extern "C"
{
#endif


#include "list_basic.h"
#include <stddef.h>
#include <stdint.h>


typedef struct list_mem
{
    struct list_mem* first;
    uint8_t* mem;
    size_t num;
    size_t size;
} list_mem_t;


void list_mem_init(list_mem_t* self, void* node_mem, size_t node_num, size_t node_size);
void* list_mem_alloc(list_mem_t* self);
void list_mem_free(list_mem_t* self, void* node);


#ifdef __cplusplus
}
#endif


#endif
