#include "list.h"
#include <assert.h>
#include <stddef.h>


void list_init(list_t* self)
{
    assert(self);

    self->first = NULL;
    self->last = NULL;
    self->length = 0;
}


void list_insert(list_t* self, void* node, uint_least32_t pos)
{
    assert(self);

    if (self->length == 0)
    {
        self->first = list_basic_insert(NULL, node, pos);
        self->last = self->first;
    }
    else
    {
        if (pos < self->length)
        {
            self->first = list_basic_insert(self->first, node, pos);
        }
        else
        {
            list_basic_insert_after(self->last, node);
            self->last = node;
        }
    }

    self->length++;
}


void list_remove(list_t* self, uint_least32_t pos)
{
    assert(self);

    if (self->length == 0)
    {
        return;
    }

    if (self->length == 1)
    {
        self->first = NULL;
        self->last = NULL;
    }
    else
    {
        if (pos < self->length)
        {
            self->first = list_basic_remove(self->first, pos);
        }
        else
        {
            struct list_node* prev = list_basic_at(self->first, pos - 1);

            list_basic_remove_next(prev);

            self->last = prev;
        }
    }

    self->length--;
}


void* list_at(list_t* self, uint_least32_t pos)
{
    assert(self);
    assert(self->first);

    return list_basic_at(self->first, pos);
}


void list_sort(list_t* self, list_compare_nodes_fp cmp)
{
    assert(self);
    assert(self->first);

    self->first = list_basic_sort(self->first, cmp);
}


void list_foreach(list_t* self, list_action_fp action)
{
    assert(self);
    assert(self->first);

    list_basic_foreach(self->first, action);
}
