#include "list_basic.h"
#include "list_mem.h"
#include <assert.h>
#include <stdlib.h>


void list_mem_init(list_mem_t* self, void* node_mem, size_t node_num,
                 size_t node_size)
{
    assert(self);
    assert(node_mem);

    self->mem = node_mem;
    self->num = node_num;
    self->size = node_size;

    self->first = NULL;

    for (uint_least32_t i = 0; i < node_num; ++i)
    {
        self->first = list_basic_insert(self->first, self->mem + (i * node_size), 0);
    }
}


void* list_mem_alloc(list_mem_t* self)
{
    assert(self);

    if (!self->first)
    {
        return NULL;
    }

    void* allocated = self->first;
    self->first = list_basic_next(allocated);

    ((struct list_node*)allocated)->next = NULL;

    return allocated;
}


void list_mem_free(list_mem_t* self, void* node)
{
    assert(self);

    self->first = list_basic_insert(self->first, node, 0);
}
