#include "greatest.h"
#include "list.h"


struct test_node
{
    struct list_node node;
    int value;
};


TEST test_insert()
{
    list_t list;
    struct test_node nodes[3] = {{.value = 0}, {.value = 1}, {.value = 2}};

    list_init(&list);

    // insert 0
    list_insert(&list, &nodes[0], 0);
    ASSERT(list.length == 1U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 0);

    // insert 2 after 0
    list_insert(&list, &nodes[2], -1);
    ASSERT(list.length == 2U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 0);
    ASSERT(((struct test_node*)list_at(&list, 1))->value == 2);
    
    // insert 1 after 0
    list_insert(&list, &nodes[1], 1);
    ASSERT(list.length == 3U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 0);
    ASSERT(((struct test_node*)list_at(&list, 1))->value == 1);
    ASSERT(((struct test_node*)list_at(&list, 2))->value == 2);

    PASS();
}


TEST test_insert_first()
{
    list_t list;
    struct test_node* test_node; 
    struct test_node nodes[3] = {{.value = 0}, {.value = 1}, {.value = 2}};

    list_init(&list);

    // insert 0
    list_insert(&list, &nodes[0], 0);
    ASSERT(list.length == 1U);
    test_node = list_at(&list, 0);
    ASSERT(test_node->value == 0U);

    // insert 1
    list_insert(&list, &nodes[1], 0);
    ASSERT(list.length == 2U);
    test_node = list_at(&list, 0);
    ASSERT(test_node->value == 1U);
    test_node = list_at(&list, 1);
    ASSERT(test_node->value == 0U);

    // insert 2
    list_insert(&list, &nodes[2], 0);
    ASSERT(list.length == 3U);
    test_node = list_at(&list, 0);
    ASSERT(test_node->value == 2U);
    test_node = list_at(&list, 1);
    ASSERT(test_node->value == 1U);
    test_node = list_at(&list, 2);
    ASSERT(test_node->value == 0U);

    PASS();
}


TEST test_remove()
{
    list_t list;
    struct test_node nodes[3] = {{.value = 0}, {.value = 1}, {.value = 2}};

    list_init(&list);

    // check error case empty
    list_remove(&list, 0);
    ASSERT(list.length == 0U);

    // insert three nodes
    list_insert(&list, &nodes[2], 0);
    list_insert(&list, &nodes[1], 0);
    list_insert(&list, &nodes[0], 0);
    ASSERT(list.length == 3U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 0);
    ASSERT(((struct test_node*)list_at(&list, 1))->value == 1);
    ASSERT(((struct test_node*)list_at(&list, 2))->value == 2);

    // remove value 1
    list_remove(&list, 1);
    ASSERT(list.length == 2U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 0);
    ASSERT(((struct test_node*)list_at(&list, 1))->value == 2);
    
    // remove value 2
    list_remove(&list, 1);
    ASSERT(list.length == 1U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 0);

    // remove 0
    list_remove(&list, 0);
    ASSERT(list.length == 0U);

    // check error case empty again
    list_remove(&list, 1);
    ASSERT(list.length == 0U);

    PASS();
}


TEST test_remove_first()
{
    list_t list;
    struct test_node nodes[3] = {{.value = 0}, {.value = 1}, {.value = 2}};

    list_init(&list);

    // check error case
    list_remove(&list, 0);
    ASSERT(list.length == 0U);

    // insert nodes
    list_insert(&list, &nodes[2], 0);
    list_insert(&list, &nodes[1], 0);
    list_insert(&list, &nodes[0], 0);
    ASSERT(list.length == 3U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 0);
    ASSERT(((struct test_node*)list_at(&list, 1))->value == 1);
    ASSERT(((struct test_node*)list_at(&list, 2))->value == 2);

    // remove value 0
    list_remove(&list, 0);
    ASSERT(list.length == 2U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 1);
    ASSERT(((struct test_node*)list_at(&list, 1))->value == 2);
    
    // remove value 1
    list_remove(&list, 0);
    ASSERT(list.length == 1U);
    ASSERT(((struct test_node*)list_at(&list, 0))->value == 2);

    // remove value 2
    list_remove(&list, 0);
    ASSERT(list.length == 0U);

    // check error case empty again
    list_remove(&list, -1);
    ASSERT(list.length == 0U);

    PASS();
}


SUITE(list)
{
    RUN_TEST(test_insert);
    RUN_TEST(test_insert_first);
    RUN_TEST(test_remove);
    RUN_TEST(test_remove_first);
}
