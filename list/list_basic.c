#include "list_basic.h"
#include <assert.h>
#include <stddef.h>


void* list_basic_insert(void* ref, void* node, uint_least32_t pos)
{
    assert(node);

    if (pos == 0)
    {
        ((struct list_node*)node)->next = ref;

        return node;
    }
       
    assert(ref);

    list_basic_insert_after(list_basic_at(ref, pos - 1), node);

    return ref;
}


void list_basic_insert_after(void* ref, void* node)
{
    assert(ref);
    assert(node);

    ((struct list_node*)node)->next = ((struct list_node*)ref)->next;
    ((struct list_node*)ref)->next = (struct list_node*)node;
}


void* list_basic_remove(void* ref, uint_least32_t pos)
{
    assert(ref);

    if (pos == 0)
    {
        return ((struct list_node*)ref)->next;
    }

    list_basic_remove_next(list_basic_at(ref, pos - 1));

    return ref;
}


void* list_basic_remove_next(void* ref)
{
    if (ref)
    {
        struct list_node* rm_node = ((struct list_node*)ref)->next;

        if (rm_node)
        {
            ((struct list_node*)ref)->next = rm_node->next;
            return rm_node;
        }
    }

    return NULL;
}


void* list_basic_next(void* ref)
{
    assert(ref);

    return ((struct list_node*)ref)->next;
}


//void* list_basic_prev(void* ref, void* node)
//{
//    assert(ref);
//    assert(ref != node);
//
//    while (((struct list_node*)ref)->next)
//    {
//        if (((struct list_node*)ref)->next == node)
//        {
//            return ref;
//        }
//
//        ref = ((struct list_node*)ref)->next;
//    }
//
//    return NULL;
//}


void* list_basic_last(void* ref)
{
    assert(ref);

    while (((struct list_node*)ref)->next)
    {
        ref = ((struct list_node*)ref)->next;
    }

    return ref;
}


void* list_basic_at(void* ref, uint_least32_t pos)
{
    assert(ref);

    for (uint_least32_t i = 0; ((struct list_node*)ref)->next && i < pos;
         ++i)
    {
        ref = ((struct list_node*)ref)->next;
    }

    return ref;
}


int_least32_t list_basic_node_pos(void* ref, void* node)
{
    int_least32_t i;

    for (i = 0; ref && ((struct list_node*)ref)->next != node; ++i)
    {
        ref = ((struct list_node*)ref)->next;
    }

    if (ref)
    {
        return i;
    }

    return -1;
}


uint_least32_t list_basic_length(void* ref)
{
    if (!ref)
    {
        return 0;
    }

    uint_least32_t length;

    for (length = 1; ((struct list_node*)ref)->next; ++length)
    {
        ref = ((struct list_node*)ref)->next;
    }

    return length;
}


void* list_basic_insert_sorted(void* ref, void* node, list_compare_nodes_fp cmp)
{
    assert(ref);
    assert(node);
    assert(cmp);

    if (cmp(node, ref) < 0)
    //if (cmp(node, ref) > 0)
    {
        ((struct list_node*)node)->next = ref;
        return node;
    }

    struct list_node* sorted = ref;

    while (sorted)
    {
        if (sorted->next && cmp(node, sorted->next) > 0)
        //if (sorted->next && cmp(node, sorted->next) <= 0)
        {
            sorted = sorted->next;
        }
        else
        {
            list_basic_insert_after(sorted, node);
            break;
        }
    }

    return ref;
}


void* list_basic_sort(void* ref, list_compare_nodes_fp cmp)
{
    struct list_node* sorted = ref;

    while (sorted)
    {
        struct list_node* next = sorted->next;

        if (next && cmp(next, sorted) < 0)
        //if (next && cmp(next, sorted) > 0)
        {
            sorted->next = next->next;
            ref = list_basic_insert_sorted(ref, next, cmp);
        }
        else
        {
            sorted = next;
        }
    }

    return ref;
}


void list_basic_foreach(void* ref, list_action_fp action)
{
    while (ref)
    {
        if (action(ref))
        {
            break;
        }

        ref = ((struct list_node*)ref)->next;
    }
}
