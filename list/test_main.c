#include "greatest.h"


GREATEST_MAIN_DEFS();


extern SUITE(list_basic);
extern SUITE(list_mem);
extern SUITE(list);


int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_SUITE(list_basic);
    RUN_SUITE(list_mem);
    RUN_SUITE(list);

    GREATEST_MAIN_END();
}
