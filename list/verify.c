#include "list.h"
#include <assert.h>
#include <stdint.h>


#define MAX_NODES 5


struct vc_list_node 
{
    struct list_node* next;
    uint32_t value;
};


struct vc_list_node nodes[MAX_NODES];


uint32_t nondet_uint32(void);


void vc_list(void)
{
    for (int i = 0; i < MAX_NODES; i++)
    {
        nodes[i].next = nondet_uint32();
        nodes[i].value = nondet_uint32();
    }

    list_t list;

    list_init(&list);

    list_insert(&list, &nodes[0], 0); // TODO: change list behaviour

    for (uint32_t i = 1; i < MAX_NODES; i++)
    {
        uint32_t node_num = nondet_uint32() % MAX_NODES;

        list_insert(&list, &nodes[node_num], nondet_uint32());
    }
}


int main(void)
{
    vc_list();

    return 0;
}
