/** \file
 * Singly linked list.
 */


#ifndef LIST_H
#define LIST_H


#ifdef __cplusplus
extern "C"
{
#endif


#include "list_basic.h"
#include <stdbool.h>
#include <stdint.h>


typedef struct list {
    struct list_node* first;
    struct list_node* last;
    uint_least32_t length;
} list_t;


void list_init(list_t* self);
void list_insert(list_t* self, void* node, uint_least32_t pos);
void list_remove(list_t* self, uint_least32_t pos);
void* list_at(list_t* self, uint_least32_t pos);
uint_least32_t list_length(list_t* list);


#ifdef __cplusplus
}
#endif


#endif
