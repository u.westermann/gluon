#include "list_basic.h"
#include "greatest.h"
#include <stdio.h>


struct test_node
{
    struct list_node node;
    int value;
    int value2;
};


static int compare(void* node1, void* node2)
{
    struct test_node* n1 = node1;
    struct test_node* n2 = node2;

    if (n1->value < n2->value)
    {
        return -1;
    }
    else if (n1->value == n2->value)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}


static int action_print(void* node)
{
    struct test_node* n = node;

    printf("\naddr: %p, next: %p, value: %d, value2: %d", (void*)n,
           (void*)((struct list_node*)node)->next, n->value, n->value2);

    return 0;
}


TEST test_basic_insert()
{
    struct test_node* first;

    struct test_node node1 = {{.next = (void*)0xDEAD}, .value = 1};
    struct test_node node2 = {{.next = (void*)0xDEAD}, .value = 2};
    struct test_node node3 = {{.next = (void*)0xDEAD}, .value = 3};
    struct test_node node4 = {{.next = (void*)0xDEAD}, .value = 4};
    struct test_node node5 = {{.next = (void*)0xDEAD}, .value = 5};

    // insert into empty list
    first = list_basic_insert(NULL, &node3, 0);
    ASSERT(first);
    ASSERT(first->value == 3);
    ASSERT(list_basic_length(first) == 1);

    // insert before first
    first = list_basic_insert(first, &node1, 0);
    ASSERT(first);
    ASSERT(first->value == 1);
    ASSERT(list_basic_length(first) == 2);

    // insert between
    first = list_basic_insert(first, &node2, 1);
    ASSERT(first);
    ASSERT(first->value == 1);
    ASSERT(list_basic_length(first) == 3);

    // insert at exact end
    first = list_basic_insert(first, &node4, 3);
    ASSERT(first);
    ASSERT(first->value == 1);
    ASSERT(list_basic_length(first) == 4);

    // insert behind end
    first = list_basic_insert(first, &node5, -1);
    ASSERT(first);
    ASSERT(first->value == 1);
    ASSERT(list_basic_length(first) == 5);

    list_basic_foreach(first, action_print);

    ASSERT(((struct test_node*)list_basic_at(first, 0))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 1))->value == 2);
    ASSERT(((struct test_node*)list_basic_at(first, 2))->value == 3);
    ASSERT(((struct test_node*)list_basic_at(first, 3))->value == 4);
    ASSERT(((struct test_node*)list_basic_at(first, 4))->value == 5);

    PASS();
}


TEST test_basic_insert_sorted_ascending()
{
    struct test_node* first;

    struct test_node node0 = {{.next = (void*)0xDEAD}, .value = 0};
    struct test_node node1 = {{.next = NULL}, .value = 1};
    struct test_node node2 = {{.next = (void*)0xDEAD}, .value = 2};
    struct test_node node3 = {{.next = (void*)0xDEAD}, .value = 3};
    struct test_node node4 = {{.next = (void*)0xDEAD}, .value = 4};

    // insert already sorted values
    first = list_basic_insert_sorted(&node1, &node3, &compare);
    ASSERT(first);
    ASSERT(first->value == 1);
    ASSERT(list_basic_length(first) == 2);

    // insert before current first node
    first = list_basic_insert_sorted(first, &node0, &compare);
    ASSERT(first);
    ASSERT(first->value == 0);
    ASSERT(list_basic_length(first) == 3);

    // insert between two existing nodes
    first = list_basic_insert_sorted(first, &node2, &compare);
    ASSERT(first);
    ASSERT(first->value == 0);
    ASSERT(list_basic_length(first) == 4);

    // insert at end
    first = list_basic_insert_sorted(first, &node4, &compare);
    ASSERT(first);
    ASSERT(first->value == 0);
    ASSERT(list_basic_length(first) == 5);

    list_basic_foreach(first, action_print);

    // assert that values are sorted in ascending order
    ASSERT(((struct test_node*)list_basic_at(first, 0))->value == 0);
    ASSERT(((struct test_node*)list_basic_at(first, 1))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 2))->value == 2);
    ASSERT(((struct test_node*)list_basic_at(first, 3))->value == 3);
    ASSERT(((struct test_node*)list_basic_at(first, 4))->value == 4);

    PASS();
}


TEST test_basic_insert_sorted_descending()
{
    SKIP();

    struct test_node* first;

    struct test_node node0 = {{.next = (void*)0xDEAD}, .value = 0};
    struct test_node node1 = {{.next = (void*)0xDEAD}, .value = 1};
    struct test_node node2 = {{.next = (void*)0xDEAD}, .value = 2};
    struct test_node node3 = {{.next = NULL}, .value = 3};
    struct test_node node4 = {{.next = (void*)0xDEAD}, .value = 4};

    // insert already sorted values
    first = list_basic_insert_sorted(&node3, &node1, &compare);
    ASSERT(first);
    ASSERT(first->value == 3);
    ASSERT(list_basic_length(first) == 2);

    // insert before current first node
    first = list_basic_insert_sorted(first, &node4, &compare);
    ASSERT(first);
    ASSERT(first->value == 4);
    ASSERT(list_basic_length(first) == 3);

    // insert between two existing nodes
    first = list_basic_insert_sorted(first, &node2, &compare);
    ASSERT(first);
    ASSERT(first->value == 4);
    ASSERT(list_basic_length(first) == 4);

    // insert at end
    first = list_basic_insert_sorted(first, &node0, &compare);
    ASSERT(first);
    ASSERT(first->value == 4);
    ASSERT(list_basic_length(first) == 5);

    list_basic_foreach(first, action_print);

    // assert that values are sorted in descending order
    ASSERT(((struct test_node*)list_basic_at(first, 0))->value == 4);
    ASSERT(((struct test_node*)list_basic_at(first, 1))->value == 3);
    ASSERT(((struct test_node*)list_basic_at(first, 2))->value == 2);
    ASSERT(((struct test_node*)list_basic_at(first, 3))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 4))->value == 0);

    PASS();
}


TEST test_basic_sort_ascending()
{
    struct test_node* first;

    struct test_node node0 = {{.next = (void*)0xDEAD}, .value = 0};
    struct test_node node1_1 = {{.next = (void*)0xDEAD}, .value = 1, .value2 = 1};
    struct test_node node1_2 = {{.next = (void*)0xDEAD}, .value = 1, .value2 = 2};
    struct test_node node1_3 = {{.next = (void*)0xDEAD}, .value = 1, .value2 = 3};
    struct test_node node2 = {{.next = (void*)0xDEAD}, .value = 2};
    struct test_node node3 = {{.next = (void*)0xDEAD}, .value = 3};
    struct test_node node4 = {{.next = NULL}, .value = 4};
    
    // insert nodes unsorted: 4, 1_1, 2, 1_3, 0, 1_2, 3
    first = &node4;
    first = list_basic_insert(first, &node1_3, 1);
    first = list_basic_insert(first, &node2, 2);
    first = list_basic_insert(first, &node1_2, 3);
    first = list_basic_insert(first, &node0, 4);
    first = list_basic_insert(first, &node1_1, 5);
    first = list_basic_insert(first, &node3, 6);
    ASSERT(first);
    ASSERT(first->value == 4);
    ASSERT(list_basic_length(first) == 7);

    first = list_basic_sort(first, &compare);
    ASSERT(first);
    ASSERT(first->value == 0);
    ASSERT(list_basic_length(first) == 7);

    list_basic_foreach(first, action_print);

    // assert that values are sorted in ascending order and that algorithm is stable
    ASSERT(((struct test_node*)list_basic_at(first, 0))->value == 0);

    ASSERT(((struct test_node*)list_basic_at(first, 1))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 1))->value2 == 1);

    ASSERT(((struct test_node*)list_basic_at(first, 2))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 2))->value2 == 3);

    ASSERT(((struct test_node*)list_basic_at(first, 3))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 3))->value2 == 2);

    ASSERT(((struct test_node*)list_basic_at(first, 4))->value == 2);
    ASSERT(((struct test_node*)list_basic_at(first, 5))->value == 3);
    ASSERT(((struct test_node*)list_basic_at(first, 6))->value == 4);

    PASS();
}


TEST test_basic_sort_descending()
{
    SKIP();

    struct test_node* first;

    struct test_node node0 = {{.next = (void*)0xDEAD}, .value = 0};
    struct test_node node1_1 = {{.next = (void*)0xDEAD}, .value = 1, .value2 = 1};
    struct test_node node1_2 = {{.next = (void*)0xDEAD}, .value = 1, .value2 = 2};
    struct test_node node1_3 = {{.next = (void*)0xDEAD}, .value = 1, .value2 = 3};
    struct test_node node2 = {{.next = (void*)0xDEAD}, .value = 2};
    struct test_node node3 = {{.next = (void*)0xDEAD}, .value = 3};
    struct test_node node4 = {{.next = NULL}, .value = 4};

    // insert nodes unsorted: 4, 1_1, 2, 1_3, 0, 1_2, 3
    first = &node4;
    first = list_basic_insert(first, &node1_1, 1);
    first = list_basic_insert(first, &node2, 2);
    first = list_basic_insert(first, &node1_3, 3);
    first = list_basic_insert(first, &node0, 4);
    first = list_basic_insert(first, &node1_2, 5);
    first = list_basic_insert(first, &node3, 6);
    ASSERT(first);
    ASSERT(first->value == 4);
    ASSERT(list_basic_length(first) == 7);

    first = list_basic_sort(first, &compare);
    ASSERT(first);
    ASSERT(first->value == 4);
    ASSERT(list_basic_length(first) == 7);

    list_basic_foreach(first, action_print);

    // assert that values are sorted in descending order and that algorithm is stable
    ASSERT(((struct test_node*)list_basic_at(first, 0))->value == 4);
    ASSERT(((struct test_node*)list_basic_at(first, 1))->value == 3);
    ASSERT(((struct test_node*)list_basic_at(first, 2))->value == 2);

    ASSERT(((struct test_node*)list_basic_at(first, 3))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 3))->value2 == 1);

    ASSERT(((struct test_node*)list_basic_at(first, 4))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 4))->value2 == 3);

    ASSERT(((struct test_node*)list_basic_at(first, 5))->value == 1);
    ASSERT(((struct test_node*)list_basic_at(first, 5))->value2 == 2);

    ASSERT(((struct test_node*)list_basic_at(first, 6))->value == 0);

    PASS();
}


SUITE(list_basic)
{
    RUN_TEST(test_basic_insert);
    RUN_TEST(test_basic_insert_sorted_ascending);
    RUN_TEST(test_basic_insert_sorted_descending);
    RUN_TEST(test_basic_sort_ascending);
    RUN_TEST(test_basic_sort_descending);
}
