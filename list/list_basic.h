/** \file
 * Basic singly linked list.
 */


#ifndef LIST_BASIC_H
#define LIST_BASIC_H


#ifdef __cplusplus
extern "C"
{
#endif


#include <stdbool.h>
#include <stdint.h>


struct list_node
{
    struct list_node* next;
};


/**
  Function pointer to node comparison function.

  @param node1  First node.
  @param node2  Second node.
  @return       For ascending sort order: -1 if node1 < node2, 0 if node1 ==
  node2, 1 if node1 > node2.
*/
typedef int (*list_compare_nodes_fp)(void* node1, void* node2);
typedef int (*list_action_fp)(void* node);


/**
  Insert node.

  @param ref        Start node. Can be NULL if pos is 0.
  @param node       Node to insert. Must not be NULL.
  @param pos        Position to insert node at. 0 prepends. Values behind list
  end will append.
  @return           Return (possibly new) first node.
*/
void* list_basic_insert(void* ref, void* node, uint_least32_t pos);
void list_basic_insert_after(void* ref, void* node);


/**
  Insert node in order. List must be alread sorted or contain only one other
  node.

  @param ref        Start node.
  @param node        Node to insert.
  @param cmp        Function pointer to function used for comparision.
  end will append.
  @return           Return (possibly new) first node.
*/
void* list_basic_insert_sorted(void* ref, void* node, list_compare_nodes_fp cmp);
void* list_basic_remove(void* ref, uint_least32_t pos);
void* list_basic_remove_next(void* ref);
void* list_basic_next(void* ref);
//void* list_basic_prev(void* ref, void* node);
void* list_basic_last(void* ref);


/**
  Get node at position.

  @param ref    Start node.
  @param pos    Get node from position. 0 gets the first node. Values behind the
  list end will address the last list element.
  @return           Return node at pos. NULL if ref is NULL.
*/
void* list_basic_at(void* ref, uint_least32_t pos);
int_least32_t list_basic_node_pos(void* ref, void* node);
uint_least32_t list_basic_length(void* ref);
void* list_basic_sort(void* ref, list_compare_nodes_fp cmp);
void list_basic_foreach(void* ref, list_action_fp action);


#ifdef __cplusplus
}
#endif


#endif
