#ifndef FLETCHER32_CHECKSUM_HEADER_ONLY
    #include "fletcher32_checksum.h"
#endif

#include <assert.h>
#include <stddef.h>

FLETCHER32_CHECKSUM_SPEC void fletcher32_checksum(uint16_t c0_init_value, uint16_t c1_init_value, uint16_t* data, uint32_t length, uint16_t* c0_result, uint16_t* c1_result) {
    assert(data != NULL);

    uint_fast32_t c0 = c0_init_value;
    uint_fast32_t c1 = c1_init_value;

    for (uint_fast32_t i = 0; i < length; ++i)
    {
        c0 = (c0 + data[i]) % 0xFFFF;
        c1 = (c1 + c0) % 0xFFFF;
    }

    *c0_result = c0;
    *c1_result = c1;
}
