#include "fletcher32_checksum.h"
#include <stdint.h>

void verification_case_checksum(void) {
    uint16_t test_vector[5];
    uint32_t length = sizeof(test_vector)/sizeof(uint16_t);

    uint16_t c0;
    uint16_t c1;

    fletcher32_checksum(0, 0, test_vector, length, &c0, &c1);
}

int main(void) {
    verification_case_checksum();

    return 0;
}
