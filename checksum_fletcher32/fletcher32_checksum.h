/** \file
 * Calculation of Fletcher32 checksum. Fletcher checksums are faster to calculate than CRCs, but are found to detect less errors.
 *
 * https://en.wikipedia.org/wiki/Fletcher's_checksum
 */

#ifndef FLETCHER32_CHECKSUM_H
#define FLETCHER32_CHECKSUM_H

/** CONFIGURATION: Define for header only library, undefine for standalone module. */
//#define FLETCHER32_CHECKSUM_HEADER_ONLY

#ifdef FLETCHER32_CHECKSUM_HEADER_ONLY
    #define FLETCHER32_CHECKSUM_SPEC static inline
#else
    #define FLETCHER32_CHECKSUM_SPEC
#endif

#include <stdint.h>

/**
 * Calculate Fletcher32 checksum. Init values are normally set to 0. result arguments contain the result after calculation. The function can be used to calculate the checksum over data of a fixed length. It is also possible to calculate the checksum of a stream of arbitrary length on the fly, using the result from the last calculation as init values for the calculation of the next results.
 */
FLETCHER32_CHECKSUM_SPEC void fletcher32_checksum(uint16_t c0_init_value, uint16_t c1_init_value, uint16_t* data, uint32_t length, uint16_t* c0_result, uint16_t* c1_result);

#ifdef FLETCHER32_CHECKSUM_HEADER_ONLY
    #include "fletcher32_checksum.c"
#endif

#endif
