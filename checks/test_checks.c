#include "greatest.h"
#include "checks.h"


TEST test_checks()
{

    PASS();
}


SUITE(checks)
{
    RUN_TEST(test_checks);
}


GREATEST_MAIN_DEFS();


int main(int argc, char** argv)
{
    GREATEST_MAIN_BEGIN();

    RUN_SUITE(checks);

    GREATEST_MAIN_END();
}
