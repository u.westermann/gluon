/** \file
 * Checks for success of arithmetic operations.
 */


#ifndef CHECKS_H
#define CHECKS_H


#ifdef __cplusplus
extern "C"
{
#endif


#include <stdint.h>


// addition a + b results in unsigned integer wrap
#define ADD_UINT64_WRAPS(a, b) (UINT64_MAX - a < b)
#define ADD_UINT32_WRAPS(a, b) (UINT32_MAX - a < b)
#define ADD_UINT16_WRAPS(a, b) (UINT16_MAX - a < b)
#define ADD_UINT8_WRAPS(a, b) (UINT8_MAX - a < b)

// subtraction a - b results in unsigned integer wrap
#define SUB_U_WRAPS(a, b) (a < b)

// cast of a to next smaller type would lose data
#define UINT64_TO_UINT32_LOSS(a) (a > UINT32_MAX)
#define UINT32_TO_UINT16_LOSS(a) (a > UINT16_MAX)
#define UINT16_TO_UINT8_LOSS(a) (a > UINT8_MAX)

// cast from unsigned to signed would would lose data
#define UINT64_TO_INT64_LOSS(a) (a > INT64_MAX)
#define UINT32_TO_INT32_LOSS(a) (a > INT32_MAX)
#define UINT16_TO_INT16_LOSS(a) (a > INT16_MAX)
#define UINT8_TO_INT8_LOSS(a) (a > INT8_MAX)


#ifdef __cplusplus
}
#endif


#endif
